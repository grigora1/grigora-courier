import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/services/http_services.dart';
import 'package:courier_grigora/utils/locator.dart';

abstract class AuthServices {
  Future<dynamic> doRegister(Map<String, dynamic> data);
  Future<dynamic> doLogin(Map<String, dynamic> data);
  Future<dynamic> doOTPVerification(Map<String, dynamic> data);
  Future<dynamic> doResendOTP(Map<String, dynamic> data);
}

class AuthServicesImpl implements AuthServices {
  final _httpServices = locator<HttpServices>();
  @override
  Future<dynamic> doRegister(Map<String, dynamic> data) async {
    return _httpServices.postHttp(kEndpointRegister, data: data);
  }

  @override
  Future doLogin(Map<String, dynamic> data) {
    return _httpServices.postHttp(kEndpointLogin, data: data);
  }

  @override
  Future doOTPVerification(Map<String, dynamic> data) {
    return _httpServices.postHttp(kEndpointVerifyOTP, data: data);
  }

  @override
  Future doResendOTP(Map<String, dynamic> data) {
    return _httpServices.putHttp(kEndpointResendOTP, data: data);
  }
}
