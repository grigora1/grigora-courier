import 'package:flutter/widgets.dart' show GlobalKey, NavigatorState;
import 'package:get/get.dart';
import 'package:courier_grigora/utils/log_printer.dart';

abstract class NavigationService {
  GlobalKey<NavigatorState> get navigatorKey;

  Future<dynamic> pushNamed(String routeName, {Object arguments});

  void pop({dynamic returnValue});
}

class NavigationServiceImpl implements NavigationService {
  final _log = getLogger("navigation_service.dart");

  @override
  GlobalKey<NavigatorState> get navigatorKey => Get.key;

  @override
  Future pushNamed(String routeName, {Object? arguments}) {
    _log.w('pushNamed: route:$routeName arguments:$arguments');
    return navigatorKey.currentState!
        .pushNamed(routeName, arguments: arguments);
  }

  @override
  void pop({returnValue}) {
    _log.w('pop: returnValue:$returnValue');
    navigatorKey.currentState!.pop(returnValue);
  }
}
