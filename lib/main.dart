import 'package:courier_grigora/views/dashboard/dashboard_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/app_pages.dart';
import 'package:courier_grigora/ui_widgets/ui_utils.dart';
import 'package:courier_grigora/utils/locator.dart';
import 'package:courier_grigora/services/navigation_service.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await setupLocator();
  // setupLogger();

  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(new MyApp());
  });
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  final navigationService = locator<NavigationService>();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: kAppName,
        theme: kThemeData,
        // darkTheme: kThemeDataDark,
        debugShowCheckedModeBanner: false,
        navigatorKey: UiUtils.navigatorKey(),
        home: DashboardScreen(),
        onGenerateRoute: (RouteSettings settings) =>
            AppPages.generateRoute(settings));
  }
}
