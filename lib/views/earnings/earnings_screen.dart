import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/dashboard_widgets.dart';
import 'package:courier_grigora/ui_widgets/earnings_widgets.dart';
import 'package:courier_grigora/ui_widgets/layouts/layout_without_back.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:courier_grigora/ui_widgets/ui_utils.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';

class EarningsScreen extends StatelessWidget {
  const EarningsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutWithoutBack(
      title: 'Earnings',
      body: Container(
        width: kWidthFull(context),
        child: Column(
          children: [
            DashboardCustomTitle(title: 'N 859,000', subtitle: 'Current week'),
            _NumberOfHours().paddingTop(kSpacingSmall),
            EarningsCol2Table(
              leftTitle: 'Today',
              leftValue: 'N 859,000',
              rightTitle: 'Balance',
              rightValue: 'N 859,000',
            ),
            HorizontalDivider(),
            DashboardTextArrow(text: 'View bank details')
                .paddingSymmetric(vertical: kSpacingMedium)
                .onTap(() => UiUtils.goto(kRouteBankDetails)),
            HorizontalDivider(),
            _TabsWidget().paddingTop(kSpacingMedium)
          ],
        ).paddingAll(kSpacingMedium).paddingBottom(kSpacingLarge),
      ),
    );
  }
}

class _NumberOfHours extends StatelessWidget {
  const _NumberOfHours({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: new TextSpan(
        style: kBodyText1Style,
        children: <TextSpan>[
          TextSpan(
              text: '39 ',
              style: new TextStyle(
                color: kColorPrimary,
              )),
          TextSpan(
            text: 'deliveries, ',
          ),
          TextSpan(
              text: '32 ',
              style: new TextStyle(
                color: kColorPrimary,
                decoration: TextDecoration.underline,
              )),
          TextSpan(
            text: 'online hours',
          ),
        ],
      ),
    );
  }
}

class _TabsWidget extends StatelessWidget {
  const _TabsWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _items = [
      {
        'title': 'Today',
        'subtitle': '12 Deliveries',
        'value': 'N 859,000',
      },
      {
        'title': 'Yesterday',
        'subtitle': '12 Deliveries',
        'value': 'N 859,000',
      },
      {
        'title': 'Dec 3',
        'subtitle': '12 Deliveries',
        'value': 'N 859,000',
      },
      {
        'title': 'Dec 2',
        'subtitle': '12 Deliveries',
        'value': 'N 859,000',
      },
      {
        'title': 'Today',
        'subtitle': '12 Deliveries',
        'value': 'N 859,000',
      },
    ];
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _Tabs(),
        HorizontalDivider().paddingTop(kSpacingMedium),
        Column(
          children: UiUtils.grigoraDynamicWidget(
              items: _items,
              builder: (i) {
                return EarningItem(
                  title: _items[i]['title'] ?? '',
                  subtitle: _items[i]['subtitle'] ?? '',
                  value: _items[i]['value'] ?? '',
                  isLastItem: _items.length - 1 == i,
                ).onTap(() => UiUtils.goto(kRouteEarningsDaily));
              }),
        )
      ],
    );
  }
}

class _Tabs extends StatelessWidget {
  const _Tabs({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _items = ['Daily', 'Weekly', 'Monthly'];
    int _current = 0;
    return Row(
      children: UiUtils.grigoraDynamicWidget(
          items: _items,
          builder: (position) {
            bool _isActive = _current == position;
            return Container(
                    decoration: BoxDecoration(
                        borderRadius: kBorderRadius,
                        color: _isActive ? kColorPrimary : Colors.transparent),
                    child: GrigoraText(
                      _items[position],
                      textColor: _isActive ? kColorWhite : kColorPrimaryText,
                      textSize: kFontSizeSmall,
                    ).paddingAll(kSpacingSmall))
                .paddingRight(kSpacingMedium);
          }),
    );
  }
}
