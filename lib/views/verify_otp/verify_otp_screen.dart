import 'package:flutter/material.dart';
import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/models/args.dart';
import 'package:courier_grigora/ui_widgets/buttons.dart';
import 'package:courier_grigora/ui_widgets/layouts/simple_layout.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:courier_grigora/utils/base_widget.dart';
import 'package:courier_grigora/utils/form_item.dart';
import 'package:courier_grigora/utils/validators.dart';
import 'package:courier_grigora/views/verify_otp/verify_otp_vm.dart';
import 'package:provider/provider.dart';
import 'package:nb_utils/nb_utils.dart';

class VerifyOTP extends StatelessWidget {
  final ArgsVerifyOTP argsOTP;
  const VerifyOTP({Key? key, required this.argsOTP}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
    return BaseWidget<VerifyOTPVM>(
        // onInitState: (model) => model.startTimer(),
        model:
            VerifyOTPVM(_formKey, token: argsOTP.token, phone: argsOTP.phone),
        builder: (context, model, child) {
          return SimpleLayout(
              isLoading: model.isLoading,
              body: Padding(
                padding: kPaddingAllMedium,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GrigoraTextTitle('OTP Verification'),
                    GrigoraText(
                            'We have sent an OTP number to your email address ${argsOTP.email}')
                        .paddingTop(kSpacingMedium),
                    _Form().paddingTop(kSpacingMedium)
                  ],
                ),
              ));
        });
  }
}

class _Form extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final model = Provider.of<VerifyOTPVM>(context);
    return Form(
      key: model.formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SingleChildScrollView(
              child: Container(
                  child: FormItemOTP(
            callback: (val) => model.otpCode = val,
            validator: (_) => Validators.validateText(model.otpCode),
          ))),
          GrigoraButton(
            onTap: () => model.doValidateOTP(),
            text: 'Continue',
            borderRadius: kBorderRadius,
          ).paddingTop(kSpacingMedium),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              GrigoraText('OTP Expires in ${model.otpExpiry}'),
              _ResendOTPButton()
            ],
          ).paddingTop(kSpacingMedium)
        ],
      ),
    );
  }
}

class _ResendOTPButton extends StatelessWidget {
  const _ResendOTPButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final model = Provider.of<VerifyOTPVM>(context);
    return InkWell(
        onTap: () => model.doResendOTP(),
        child: GrigoraText('Resend OTP', textColor: kColorPrimary));
  }
}
