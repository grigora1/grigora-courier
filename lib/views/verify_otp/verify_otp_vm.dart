import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/models/otp.dart';
import 'package:courier_grigora/models/resend_otp.dart';
import 'package:courier_grigora/ui_widgets/ui_utils.dart';
import 'package:courier_grigora/utils/base_model.dart';
import 'package:courier_grigora/services/auth_services.dart';
import 'package:courier_grigora/utils/locator.dart';
import 'package:courier_grigora/utils/log_printer.dart';

class VerifyOTPVM extends BaseModel {
  final logger = getLogger("verify_otp_vm.dart");
  final GlobalKey<FormState> formKey;
  final String token, phone;

  final _authService = locator<AuthServices>();

  VerifyOTPVM(this.formKey, {required this.token, required this.phone});

  bool _isLoading = false;
  bool get isLoading => _isLoading;
  set isLoading(bool value) {
    _isLoading = value;
    notifyListeners();
  }

  String _otpCode = "";
  String get otpCode => _otpCode;
  set otpCode(String value) {
    _otpCode = value;
    notifyListeners();
  }

  String get otpExpiry => "$minutes:$seconds $unit";

  int seconds = 59;
  int minutes = 09;
  int countDownMinutes = 0;
  String? unit;

  // void startTimer() {
  //   const oneSec = const Duration(seconds: 1);
  //   _timer = new Timer.periodic(oneSec, (Timer timer) {
  //     if (seconds == 0 && minutes == 0) {
  //       timer.cancel();
  //     } else {
  //       seconds -= 1;
  //       if (seconds == 0 && minutes > 0) {
  //         minutes -= 1;
  //         seconds = 59;
  //         unit = "mins";
  //       } else {
  //         unit = "secs";
  //       }
  //     }
  //     notifyListeners();
  //   });
  // }

  Future<void> doValidateOTP() async {
    if (!formKey.currentState!.validate()) return;

    formKey.currentState!.save();

    try {
      isLoading = true;
      dynamic res = await _authService
          .doOTPVerification({"otp": otpCode, "token": token});
      ModelOTP parsedRes = ModelOTP.fromJson(res);
      UiUtils.showSimpleDialog(
          title: 'Success',
          message: parsedRes.message,
          onOkPressed: () => UiUtils.goto(kRouteLoginEmail));
    } catch (error) {
      logger.e(error.toString());

      UiUtils.showSimpleDialog(
          title: 'Failed',
          message: error.toString(),
          onOkPressed: () => UiUtils.goBack());
    } finally {
      isLoading = false;
    }
  }

  Future<void> doResendOTP() async {
    try {
      isLoading = true;
      dynamic res = await _authService.doResendOTP({"phone": phone});
      ModelResendOTP parsedRes = ModelResendOTP.fromJson(res);
      // @todo: show dialog
      UiUtils.showSnackbar(parsedRes.message ?? 'OTP Code sent to $phone');
    } catch (error) {
      logger.e(error.toString());
      UiUtils.showSnackbar(error.toString());
    } finally {
      isLoading = false;
    }
  }
}
