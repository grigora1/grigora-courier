import 'package:courier_grigora/ui_widgets/layouts/layout_with_back.dart';
import 'package:courier_grigora/ui_widgets/ui_utils.dart';
import 'package:flutter/material.dart';
import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:nb_utils/nb_utils.dart';

class ChallengesScreen extends StatelessWidget {
  const ChallengesScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutWithBack(
      title: 'Challenges',
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [_TabsWidget()],
      ).paddingAll(kSpacingMedium),
    );
  }
}

class _TabModel {
  final String title;
  final Widget body;
  _TabModel({required this.title, required this.body});
}

class _TabsWidget extends StatelessWidget {
  const _TabsWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _tabs = [
      _TabModel(title: 'New', body: _NewChallenges()),
      _TabModel(title: 'Active', body: _ActiveChallenges()),
      _TabModel(title: 'Past', body: _PastChallenges())
    ];
    return DefaultTabController(
        length: _tabs.length,
        initialIndex: 0,
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                child: TabBar(
                    labelColor: kColorPrimary,
                    unselectedLabelColor: kColorPrimaryText,
                    indicatorColor: kColorPrimary,
                    labelStyle: kHeadline1TextStyle,
                    tabs: UiUtils.grigoraDynamicWidget(
                        items: _tabs,
                        builder: (i) => Tab(text: _tabs[i].title))),
              ),
              Container(
                  height: kHeightFull(context), //height of TabBarView
                  child: TabBarView(
                      children: UiUtils.grigoraDynamicWidget(
                          items: _tabs,
                          builder: (i) => SingleChildScrollView(
                                physics: BouncingScrollPhysics(),
                                child: _tabs[i]
                                    .body
                                    .paddingTop(kSpacingMedium)
                                    .paddingBottom(kSpacingExtraLarge),
                              ))))
            ]));
  }
}

class _PastChallenges extends StatelessWidget {
  const _PastChallenges({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _items = [
      _ItemModel(
          title: 'Milestone',
          description: 'Deliver 30 more orders and \nN5000 is all yours!',
          durationTotal: '50',
          footerText: 'View Performance',
          durationFooter: 'Ended',
          durationValue: '40'),
    ];
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: UiUtils.grigoraDynamicWidget(
          items: _items,
          builder: (i) => _Item(
                title: _items[i].title,
                description: _items[i].description,
                durationTotal: _items[i].durationTotal,
                durationValue: _items[i].durationValue,
                footerText: _items[i].footerText,
                durationFooter: _items[i].durationFooter,
              )
                  .onTap(() => UiUtils.goto(kRouteChallengePast))
                  .paddingBottom(kSpacingMedium)),
    );
  }
}

class _ActiveChallenges extends StatelessWidget {
  const _ActiveChallenges({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _items = [
      _ItemModel(
          title: 'Milestone',
          description: 'Deliver 30 more orders and \nN5000 is all yours!',
          durationTotal: '50',
          footerText: 'View Progress',
          durationFooter: '4 days left',
          durationValue: '35'),
    ];
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: UiUtils.grigoraDynamicWidget(
          items: _items,
          builder: (i) => _Item(
                title: _items[i].title,
                description: _items[i].description,
                durationTotal: _items[i].durationTotal,
                durationValue: _items[i].durationValue,
                footerText: _items[i].footerText,
                durationFooter: _items[i].durationFooter,
              )
                  .onTap(() => UiUtils.goto(kRouteChallengeActive))
                  .paddingBottom(kSpacingMedium)),
    );
  }
}

class _NewChallenges extends StatelessWidget {
  const _NewChallenges({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _items = [
      _ItemModel(
          title: 'Milestone',
          description:
              'Details of the challenge goes here. It could be in milestones which is paid per each one completed.',
          durationTotal: '50',
          durationFooter: 'Duration',
          footerText: 'Join',
          durationValue: '00'),
      _ItemModel(
          title: 'Milestone',
          footerText: 'Join',
          durationFooter: 'Duration',
          description:
              'Details of the challenge goes here. It could be in milestones which is paid per each one completed.',
          durationTotal: '50',
          durationValue: '00'),
    ];
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: UiUtils.grigoraDynamicWidget(
          items: _items,
          builder: (i) => _Item(
                title: _items[i].title,
                description: _items[i].description,
                durationTotal: _items[i].durationTotal,
                durationValue: _items[i].durationValue,
                durationFooter: _items[i].durationFooter,
                footerText: _items[i].footerText,
              )
                  .onTap(() => UiUtils.goto(kRouteChallengeNew))
                  .paddingBottom(kSpacingMedium)),
    );
  }
}

class _ItemModel {
  final String title,
      description,
      durationFooter,
      footerText,
      durationTotal,
      durationValue;
  _ItemModel(
      {required this.title,
      required this.description,
      required this.durationTotal,
      required this.durationFooter,
      required this.footerText,
      required this.durationValue});
}

class _Item extends StatelessWidget {
  final String title,
      description,
      durationFooter,
      footerText,
      durationTotal,
      durationValue;
  const _Item(
      {Key? key,
      required this.title,
      required this.description,
      required this.durationFooter,
      required this.footerText,
      required this.durationTotal,
      required this.durationValue})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                GrigoraTextTitle(
                  title,
                ),
                GrigoraText(description).paddingTop(kSpacingSmall)
              ],
            ).expand(),
            Column(
              children: [
                Row(
                  children: [
                    GrigoraTextTitle(
                      durationValue,
                      textColor: kColorAccent,
                    ),
                    GrigoraTextTitle('/'),
                    GrigoraTextTitle(durationTotal),
                  ],
                ),
                GrigoraText(
                  durationFooter,
                  textSize: kFontSizeSmall,
                )
              ],
            )
          ],
        ),
        Container(
          width: kWidthFull(context),
          child: GrigoraText(
            footerText,
            textColor: kColorPrimary,
            textAlign: TextAlign.right,
          ).paddingTop(kSpacingSmall),
        )
      ],
    );
  }
}
