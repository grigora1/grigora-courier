import 'package:flutter/material.dart';
import 'package:courier_grigora/ui_widgets/layouts/layout_with_reliability.dart';

class DeclineAcceptScreen extends StatelessWidget {
  const DeclineAcceptScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutWithReliability(
      title: 'Are you sure you want to decline this order?',
      description: 'You\'re the best postman for this order.',
      reliabilityScore: '72%',
      buttonText: 'Decline',
    );
  }
}
