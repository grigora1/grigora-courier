import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/layouts/layout_with_back.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:courier_grigora/ui_widgets/ui_utils.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';

class PostmanHelpScreen extends StatelessWidget {
  const PostmanHelpScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutWithBack(
      title: 'How can we help you?',
      bgColor: kColorBg,
      body: Column(
        children: [
          _HelpSection(
            title: 'Pick up issues',
            items: ['The merchant is closed', 'Unassign this Delivery'],
          ),
          _HelpSection(
            title: 'Delivery point issues',
            items: [
              'Can\'t hand order to customer',
              'Issue with customer address'
            ],
          ).paddingTop(kSpacingMedium),
          _HelpSection(
            title: 'Get help now',
            items: [
              'I\'m not getting deliveries',
              'Issue with customer address'
            ],
          ).paddingTop(kSpacingMedium),
        ],
      ).paddingSymmetric(vertical: kSpacingMedium),
    );
  }
}

class _HelpSection extends StatelessWidget {
  final String title;
  final List<String> items;
  const _HelpSection({Key? key, required this.title, required this.items})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GrigoraText(
          title,
          isBoldFont: true,
        ).paddingSymmetric(horizontal: kSpacingMedium),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: UiUtils.grigoraDynamicWidget(
              items: items,
              builder: (i) => Container(
                      width: kWidthFull(context),
                      color: kColorWhite,
                      child: GrigoraText(items[i]).paddingAll(kSpacingMedium))
                  .paddingTop(5)),
        ).paddingTop(kSpacingSmall)
      ],
    );
  }
}
