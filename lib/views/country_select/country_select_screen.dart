import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:courier_grigora/utils/base_widget.dart';
import 'package:courier_grigora/views/country_select/contry_select_vm.dart';
import 'package:courier_grigora/views/country_select/countries.dart';
import 'package:nb_utils/nb_utils.dart';

class CountrySelectScreen extends StatelessWidget {
  final List<Map<String, String>> _countries = CountryList.countries;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<CountrySelectVM>(
      model: CountrySelectVM(),
      builder: (context, model, child) {
        return Scaffold(
          appBar: new AppBar(
            title: GrigoraTextTitle(
              'Where will we deliver to?',
              textSize: kFontSizeMedium,
            ),
            centerTitle: true,
            backgroundColor: kColorWhite,
            elevation: 0,
          ),
          body: SafeArea(
              child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: ListView.builder(
                itemCount: _countries.length,
                itemBuilder: (BuildContext context, int position) {
                  return _SelectCountryItem(
                    name: _countries[position]['name']!,
                    flag: _countries[position]['file_url']!,
                    onTap: () => kGotoScreen(context, kRouteHome),
                  );
                }),
          )),
        );
      },
    );
  }
}

class _SelectCountryItem extends StatelessWidget {
  final bool isLastItem;
  final String name, flag;
  final Function() onTap;
  const _SelectCountryItem(
      {Key? key,
      this.isLastItem = false,
      required this.onTap,
      required this.name,
      required this.flag})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Column(
        children: [
          Padding(
            padding: kPaddingAllSmall,
            child: Row(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(50),
                  child: Container(
                    width: 40,
                    height: 40,
                    child: SvgPicture.network(
                      flag,
                      semanticsLabel: name,
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
                GrigoraText(name).paddingTop(kSpacingMedium)
              ],
            ),
          ),
          Visibility(
            visible: !isLastItem,
            child: Container(
              height: 1,
              width: kWidthFull(context),
              color: kColorGrey,
            ),
          )
        ],
      ),
    );
  }
}
