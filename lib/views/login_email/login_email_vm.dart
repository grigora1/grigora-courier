import 'package:flutter/cupertino.dart';
import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/models/login.dart';
import 'package:courier_grigora/ui_widgets/ui_utils.dart';
import 'package:courier_grigora/utils/base_model.dart';
import 'package:courier_grigora/utils/locator.dart';
import 'package:courier_grigora/utils/log_printer.dart';
import 'package:courier_grigora/services/auth_services.dart';

class LoginEmailVM extends BaseModel {
  final logger = getLogger("login_email_vm.dart");
  final _authService = locator<AuthServices>();
  final GlobalKey<FormState> formKey;

  LoginEmailVM(this.formKey);

  bool _isLoading = false;
  bool get isLoading => _isLoading;
  set isLoading(bool value) {
    _isLoading = value;
    notifyListeners();
  }

  // email
  String _email = "";
  String get email => _email;
  set email(String value) {
    _email = value;
    notifyListeners();
  }

  // password
  String _password = "";
  String get password => _password;
  set password(String value) {
    _password = value;
    notifyListeners();
  }

  Future<void> doLogin() async {
    if (!formKey.currentState!.validate()) return;

    formKey.currentState!.save();

    try {
      isLoading = true;
      dynamic res =
          await _authService.doLogin({"email": email, "password": password});
      ModelLogin parsedRes = ModelLogin.fromJson(res);
      print(parsedRes);
      // @todo: save token
      UiUtils.showSnackbar('Login successful');
      UiUtils.goto(kRouteRestaurants);
    } catch (error) {
      logger.e(error.toString());
      UiUtils.showSimpleDialog(
          title: 'Failed',
          message: error.toString(),
          onOkPressed: () => UiUtils.goBack());
    } finally {
      isLoading = false;
    }
  }
}
