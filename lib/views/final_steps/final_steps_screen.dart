import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/buttons.dart';
import 'package:courier_grigora/ui_widgets/faq.dart';
import 'package:courier_grigora/ui_widgets/layouts/simple_layout.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:courier_grigora/ui_widgets/ui_utils.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';

class FinalStepsScreen extends StatelessWidget {
  const FinalStepsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SimpleLayout(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GrigoraTextTitle(
            "Hey User",
            textSize: 30,
          ),
          GrigoraText(
                  "Complete these final steps so you can start making money with Grigora!")
              .paddingTop(kSpacingMedium),
          FaqItemWidget(
                  title: 'Complete Background Check',
                  subtitle:
                      'More information is required to complete your background check. Use the link below to provide more information through the Checkr candidate portal.')
              .paddingTop(kSpacingMedium),
          FaqItemWidget(
              title: 'Add Bank Account',
              subtitle:
                  'Add your bank account to receive payment from Grigora once your background check is complete.'),
          FaqItemWidget(
              title: 'Complete First Delivery',
              subtitle:
                  'Once you complete your profile, you\'ll be ready to start dashing.'),
          GrigoraButton(
            onTap: () => UiUtils.goto(kRouteDashboard),
            text: 'Continue',
            withBg: true,
            borderRadius: kBorderRadius,
          ).paddingTop(kSpacingMedium),
        ],
      ).paddingAll(kSpacingMedium),
    );
  }
}

class _PersonalDetails extends StatelessWidget {
  const _PersonalDetails({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        GrigoraTextTitle('Personal Details'),
        Container(
          decoration:
              BoxDecoration(color: kColorGrey, borderRadius: kFullBorderRadius),
          child: Row(
            children: [
              Icon(EvaIcons.edit),
              GrigoraText('Edit').paddingRight(kSpacingSmall)
            ],
          ).paddingSymmetric(horizontal: kSpacingSmall, vertical: 5),
        )
      ],
    );
  }
}

class _FieldValue extends StatelessWidget {
  final String title, value;
  const _FieldValue({Key? key, required this.title, required this.value})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [GrigoraTextTitle(title), GrigoraText(value)],
    );
  }
}
