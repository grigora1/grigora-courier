import 'package:courier_grigora/ui_widgets/ui_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/buttons.dart';
import 'package:courier_grigora/ui_widgets/layouts/simple_layout.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:courier_grigora/utils/base_widget.dart';
import 'package:courier_grigora/utils/form_item.dart';
import 'package:courier_grigora/utils/validators.dart';
import 'package:courier_grigora/views/login/login_vm.dart';
import 'package:provider/provider.dart';
import 'package:nb_utils/nb_utils.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
    return BaseWidget<LoginVM>(
        model: LoginVM(_formKey),
        builder: (context, model, child) {
          return SimpleLayout(
              isLoading: model.isLoading,
              body: Padding(
                padding: kPaddingAllMedium,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GrigoraTextTitle('Continue with email'),
                    _SignInForm().paddingTop(kSpacingMedium)
                  ],
                ),
              ));
        });
  }
}

class _SignInForm extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final model = Provider.of<LoginVM>(context);
    return Form(
      key: model.formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          FormItem(
            placeholder: 'Email',
            onSaved: (val) => model.email = val!,
            keyboardType: EnumKeyboardTpe.email,
            withNoBorder: true,
            validator: (val) => Validators.validateEmail(val),
          ),
          Divider(
            color: kColorDarkGrey,
          ),
          FormItem(
            placeholder: 'Password',
            onSaved: (val) => model.password = val!,
            withNoBorder: true,
            password: true,
            validator: (val) => Validators.validateText(val),
          ),
          Divider(
            color: kColorDarkGrey,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              _LinkText(
                text: 'Forgot Password?',
                onTap: () => kGotoScreen(context, kRouteForgotPassword),
              ),
              _LinkText(
                text: 'Create an account',
                onTap: () => kGotoScreen(context, kRouteCreateAccount),
              ),
            ],
          ).paddingTop(kSpacingMedium),
          GrigoraButton(
            onTap: () => model.doLogin(),
            text: 'Sign in',
            withBg: true,
            borderRadius: kBorderRadius,
          ).paddingTop(kSpacingMedium),
          GrigoraButton(
            onTap: () => UiUtils.goto(kRouteCreateAccount),
            text: 'Become a Dasher',
            borderRadius: kBorderRadius,
          ).paddingTop(kSpacingMedium),
        ],
      ),
    );
  }
}

class _LinkText extends StatelessWidget {
  final String text;
  final Function() onTap;
  const _LinkText({Key? key, required this.text, required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: GrigoraText(
        text,
        textColor: kColorPrimary,
      ),
    );
  }
}
