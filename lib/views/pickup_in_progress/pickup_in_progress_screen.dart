import 'package:courier_grigora/ui_widgets/misc_widgets.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/buttons.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:courier_grigora/ui_widgets/ui_utils.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:courier_grigora/ui_widgets/layouts/layout_with_nested_scroll.dart';

class PickupInProgressScreen extends StatelessWidget {
  const PickupInProgressScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutWithNestedScroll(
        title: 'Pickup in Progress',
        bgColor: kColorBg,
        expandedHeight: MediaQuery.of(context).size.height / 1.8,
        buttonWidget: GrigoraButton(
          onTap: () => UiUtils.goto(kRouteArriveAtStore),
          text: 'Arrive at Store',
          withBg: true,
        ),
        actions: miscWidgetAppbarActions,
        backgroundWidget: Container(
          decoration: BoxDecoration(
              image: UiUtils.grigoraNetworkImageProvider(
                  'https://res.cloudinary.com/dbdpkkfaf/image/upload/v1627432324/grigora/bg_nk4n83.png')),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [PickupWidget()],
        ));
  }
}

class _PickupTime extends StatelessWidget {
  const _PickupTime({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        color: kColorBg,
        width: kWidthFull(context),
        child: GrigoraTextTitle(
          'Pickup by 3PM',
          isCenter: true,
        ).paddingAll(kSpacingMedium));
  }
}

class PickupWidget extends StatelessWidget {
  const PickupWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _PickupTime(),
        PickUpTitle(
          title: 'PICK UP',
          name: 'Burger King',
          address: '123 ddress street, \nlocated somewhere, Ikeja.',
        ).paddingBottom(kSpacingSmall),
        PickupItem(
          title: 'Order number: 12345',
          description: 'Please use this ID instead of customer name',
        ),
        PickupItem(
          title: 'Limit contact',
          description: 'Maintain space from others while you wait',
          accentColor: true,
        ),
        PickupItem(
          title: 'Instructions from Store',
          descriptionWidget: UL(
            children: [
              GrigoraText(
                  'Instruction of where and process of picking up an item left by merchant will appear here.'),
              GrigoraText(
                  'Basically what the post man need to do when they arrive at the store.'),
            ],
          ),
        ),
        PickupItem(
          title: '4 items to pickup',
        ),
      ],
    );
  }
}

class PickUpTitle extends StatelessWidget {
  final String title, name, address;
  final bool showNavigate, showComment;
  const PickUpTitle(
      {Key? key,
      required this.title,
      required this.name,
      required this.address,
      this.showComment = false,
      this.showNavigate = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: kWidthFull(context),
      color: kColorWhite,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GrigoraText(
                title,
                textColor: kColorAccent,
              ),
              GrigoraTextTitle(name),
              GrigoraText(
                address,
              ).paddingTop(kSpacingSmall),
              _Navigate().paddingTop(kSpacingSmall).visible(showNavigate)
            ],
          ),
          Row(
            children: [
              Icon(
                EvaIcons.messageCircleOutline,
                color: kColorPrimary,
                size: 24,
              ).paddingRight(kSpacingMedium).visible(showComment),
              Icon(
                EvaIcons.phoneOutline,
                color: kColorPrimary,
                size: 24,
              ),
            ],
          )
        ],
      ).paddingAll(kSpacingMedium),
    );
  }
}

class _Navigate extends StatelessWidget {
  const _Navigate({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(
          EvaIcons.compassOutline,
          size: 24,
          color: kColorPrimary,
        ),
        GrigoraText(
          'Navigate',
          textColor: kColorPrimary,
        ).paddingLeft(kSpacingSmall)
      ],
    );
  }
}

class PickupItem extends StatelessWidget {
  final String title;
  final String? description;
  final Widget? descriptionWidget;
  final bool accentColor;
  const PickupItem(
      {Key? key,
      required this.title,
      this.description,
      this.descriptionWidget,
      this.accentColor = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: accentColor ? kColorAccent.withOpacity(0.1) : kColorWhite,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Icon(
            EvaIcons.archiveOutline,
            size: 24,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GrigoraTextTitle(title),
              GrigoraText(description ?? '')
                  .paddingTop(kSpacingSmall)
                  .visible(description is String),
              Container(
                child: descriptionWidget is Widget
                    ? descriptionWidget
                    : Container(),
              ).paddingTop(kSpacingSmall).visible(descriptionWidget is Widget)
            ],
          ).paddingLeft(kSpacingSmall).expand()
        ],
      ).paddingAll(kSpacingMedium),
    ).paddingBottom(kSpacingSmall);
  }
}
