import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/buttons.dart';
import 'package:courier_grigora/ui_widgets/layouts/layout_with_back.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:courier_grigora/ui_widgets/ui_utils.dart';
import 'package:courier_grigora/utils/form_item.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';

class ScheduleSettingsScreen extends StatelessWidget {
  const ScheduleSettingsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutWithBack(
      title: 'Schedule Settings',
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          FormItem(
            label: 'Vehicle',
            placeholder: 'Bike',
          ),
          GrigoraText(
                  'Choose the regions where you might be interested for pickup')
              .paddingTop(kSpacingMedium),
          _CurrentLocation().paddingTop(kSpacingMedium),
          FormItem(
            placeholder: 'Ikeja, Lagos',
          ).paddingTop(kSpacingSmall),
          _CheckboxItem(
            label: 'Lagos: Ikeja',
          ),
          _CheckboxItem(
            label: 'Lagos: Berger',
          ),
          _CheckboxItem(
            label: 'Lagos: Lekki',
          ),
          GrigoraButton(
            onTap: () => UiUtils.goto(kRouteDashboard),
            text: 'Apply Filter',
            withBg: true,
          ).paddingTop(kSpacingMedium),
        ],
      ).paddingAll(kSpacingMedium),
    );
  }
}

class _CheckboxItem extends StatelessWidget {
  final String label;
  const _CheckboxItem({Key? key, required this.label}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Checkbox(
          value: false,
          onChanged: (value) => print(value),
        ),
        GrigoraText(
          label,
        ),
      ],
    );
  }
}

class _CurrentLocation extends StatelessWidget {
  const _CurrentLocation({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        GrigoraText(
          'Current Location',
          textColor: kColorAccent,
          isBoldFont: true,
        ).expand(),
        Icon(
          EvaIcons.compassOutline,
          size: 20,
        )
      ],
    );
  }
}
