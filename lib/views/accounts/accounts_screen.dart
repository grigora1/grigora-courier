import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/layouts/layout_without_back.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:courier_grigora/ui_widgets/ui_utils.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';

class AccountScreen extends StatelessWidget {
  const AccountScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutWithoutBack(
      title: 'Accounts',
      body: Column(
        children: [
          _HeaderWidget(),
          _ReferAFriend().paddingTop(kSpacingBigMedium),
          _AccountSetting().paddingTop(kSpacingBigMedium),
          _NavigationSettings().paddingTop(kSpacingBigMedium),
          _LogoutWidget().paddingTop(kSpacingMedium)
        ],
      )
          .paddingSymmetric(horizontal: kSpacingMedium)
          .paddingBottom(kSpacingLarge),
    );
  }
}

class _HeaderWidget extends StatelessWidget {
  const _HeaderWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: kWidthFull(context),
      decoration:
          BoxDecoration(color: kColorWhite, borderRadius: kBorderRadius),
      child: Stack(
        children: [
          Column(
            children: [
              Container(
                width: 80,
                height: 80,
                decoration: BoxDecoration(
                    color: kColorBlack.withOpacity(0.5),
                    borderRadius: kFullBorderRadius),
              ),
              GrigoraTextTitle(
                'Nate Fobs',
                textSize: kFontSizeBig,
              ).paddingTop(kSpacingSmall),
              GrigoraText('Nattyfobs@dzooco | (+234) 793028275')
                  .paddingTop(kSpacingSmall),
              _Tag('USER ID: 927549735DT').paddingTop(kSpacingSmall),
              Divider(
                color: kColorBlack,
              ),
              _HeaderRowWidget(
                activeHours: '12.5',
                rating: '4.5',
                hoursOnline: '20',
                totalDeliveries: '40',
              )
            ],
          ).paddingAll(kSpacingMedium),
          Positioned(
              right: 30,
              top: 30,
              child:
                  GrigoraText('Edit').onTap(() => UiUtils.goto(kRouteProfile)))
        ],
      ),
    );
  }
}

class _Tag extends StatelessWidget {
  final String text;
  const _Tag(this.text, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: kColorGrey, borderRadius: kBorderRadius),
      child: GrigoraText(
        text,
        textSize: kFontSizeSmall,
      ).paddingSymmetric(vertical: 5, horizontal: kSpacingSmall),
    );
  }
}

class _HeaderRowWidget extends StatelessWidget {
  final String activeHours, rating, hoursOnline, totalDeliveries;
  const _HeaderRowWidget(
      {Key? key,
      required this.activeHours,
      required this.rating,
      required this.hoursOnline,
      required this.totalDeliveries})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        _HeaderRowWidgetItem(
          title: 'Active Hours',
          value: activeHours,
        ),
        _HeaderRowWidgetItem(
          title: 'Rating',
          value: rating,
        ),
        _HeaderRowWidgetItem(
          title: 'Hours Online',
          value: hoursOnline,
        ),
        _HeaderRowWidgetItem(
          title: 'Total Deliveries',
          value: totalDeliveries,
          isLastItem: true,
        ),
      ],
    );
  }
}

class _HeaderRowWidgetItem extends StatelessWidget {
  final String title, value;
  final bool isLastItem;
  const _HeaderRowWidgetItem(
      {Key? key,
      required this.title,
      required this.value,
      this.isLastItem = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Column(
          children: [
            GrigoraText(
              value,
              isBoldFont: true,
            ),
            GrigoraText(
              title,
              textSize: kFontSizeSmall,
            )
          ],
        ),
        Container(
          color: kColorGrey,
          width: 1,
          height: 25,
        ).paddingSymmetric(horizontal: kSpacingSmall).visible(!isLastItem)
      ],
    );
  }
}

class _ReferAFriend extends StatelessWidget {
  const _ReferAFriend({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GrigoraTextTitle(
              'Refer a friend',
              textSize: kFontSizeMedium,
              textColor: kColorAccent,
            ),
            GrigoraText(
              'Earn N10,000',
              textSize: kFontSizeSmall,
            )
          ],
        ).expand(),
        Icon(
          EvaIcons.chevronRight,
          size: 30,
        )
      ],
    ).onTap(() => UiUtils.goto(kRouteReferral));
  }
}

class _AccountSetting extends StatelessWidget {
  const _AccountSetting({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GrigoraTextTitle('Account Setting'),
        _AccountSettingItem(
          title: 'Bank Information',
          subtitle: 'Account details',
          icon: EvaIcons.creditCardOutline,
        ).onTap(() => UiUtils.goto(kRouteBankDetails)),
        _AccountSettingItem(
          title: 'Linked Vehicles',
          icon: EvaIcons.carOutline,
        ).onTap(() => UiUtils.goto(kRouteVehicleInformation)),
        _AccountSettingItem(
          title: 'Contact support',
          icon: EvaIcons.carOutline,
        ),
      ],
    );
  }
}

class _AccountSettingItem extends StatelessWidget {
  final String title;
  final String? subtitle;
  final bool isLastItem;
  final IconData icon;
  const _AccountSettingItem(
      {Key? key,
      required this.title,
      required this.icon,
      this.isLastItem = false,
      this.subtitle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Icon(icon),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GrigoraText(
              title,
              isBoldFont: true,
            ),
            GrigoraText(
              subtitle ?? '',
              textSize: kFontSizeSmall,
            ).paddingTop(kSpacingSmall).visible(subtitle != null),
          ],
        ).paddingLeft(kSpacingSmall).expand(),
        Icon(
          EvaIcons.chevronRight,
          size: 30,
        )
      ],
    ).paddingTop(kSpacingMedium);
  }
}

class _NavigationSettings extends StatelessWidget {
  const _NavigationSettings({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GrigoraTextTitle('Navigation Settings'),
        GrigoraText(
          'MAPPING SERVICE',
          textSize: kFontSizeSmall,
        ).paddingTop(kSpacingSmall).paddingBottom(kSpacingSmall),
        _NavigationSettingsItem(
          title: 'Google Maps',
        ),
        _NavigationSettingsItem(
          title: 'Apple Maps',
        ),
        _NavigationSettingsItem(
          title: 'Others',
        ),
      ],
    );
  }
}

class _NavigationSettingsItem extends StatelessWidget {
  final String title;
  final bool isLastItem;
  const _NavigationSettingsItem(
      {Key? key, required this.title, this.isLastItem = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            // Icon(icon),
            Container(
              width: 50,
              height: 50,
              decoration: BoxDecoration(
                  color: kColorBlack.withOpacity(0.2),
                  borderRadius: kFullBorderRadius),
            ),

            GrigoraText(
              title,
              isBoldFont: true,
            ).paddingLeft(kSpacingSmall).expand(),
          ],
        ),
        Divider(
          color: kColorBlack.withOpacity(0.4),
        ).visible(!isLastItem)
      ],
    );
  }
}

class _LogoutWidget extends StatelessWidget {
  const _LogoutWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GrigoraText(
          'Logout',
          textColor: kColorPrimary,
          isBoldFont: true,
        ).expand(),
        Icon(
          EvaIcons.chevronRight,
          size: 30,
        )
      ],
    );
  }
}
