import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/buttons.dart';
import 'package:courier_grigora/ui_widgets/dashboard_widgets.dart';
import 'package:courier_grigora/ui_widgets/earnings_widgets.dart';
import 'package:courier_grigora/ui_widgets/layouts/layout_with_back.dart';
import 'package:courier_grigora/ui_widgets/misc_widgets.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:courier_grigora/ui_widgets/ui_utils.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';

class EarningsDailyScreen extends StatelessWidget {
  const EarningsDailyScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutWithBack(
      title: 'Daily Earnings',
      body: Container(
        width: kWidthFull(context),
        child: Column(
          children: [
            DashboardCustomTitle(title: 'N 19,000', subtitle: 'Today'),
            _NumberOfHours().paddingTop(kSpacingSmall),
            EarningsCol2Table(
              leftTitle: 'Deliveries',
              leftValue: '4',
              rightTitle: 'Active Hours',
              rightValue: '2.5',
            ),
            Image.asset(kImagesDailyEarning),
            HorizontalDivider().paddingTop(kSpacingMedium),
            DashboardTextArrow(text: 'Get paid faster: Learn more')
                .paddingTop(kSpacingMedium),
            HorizontalDivider().paddingTop(kSpacingMedium),
            _Items()
          ],
        ).paddingAll(kSpacingMedium),
      ),
    );
  }
}

class _Items extends StatelessWidget {
  const _Items({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _items = [
      {
        'title': 'Radison BLU Ikeja',
        'subtitle': '2.6 km 12:00pm',
        'value': 'N 859,000',
      },
      {
        'title': 'Yakoyo Restaurant',
        'subtitle': '2.6 km 12.00pm',
        'value': 'N 5,000',
      },
      {
        'title': 'Shoprite Alausa',
        'subtitle': '2.6 km 12.00pm',
        'value': 'N 2,500',
      },
    ];
    return Column(
      children: UiUtils.grigoraDynamicWidget(
          items: _items,
          builder: (i) {
            return EarningItem(
              title: _items[i]['title'] ?? '',
              subtitle: _items[i]['subtitle'] ?? '',
              value: _items[i]['value'] ?? '',
              isLastItem: _items.length - 1 == i,
            ).onTap(() => UiUtils.showBottomSheetModal(
                context: context, widget: MiscWidgetDeliveryCompleteItems()));
          }),
    );
  }
}

class _NumberOfHours extends StatelessWidget {
  const _NumberOfHours({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: new TextSpan(
        style: kBodyText1Style,
        children: <TextSpan>[
          TextSpan(
              text: '3 ',
              style: new TextStyle(
                color: kColorPrimary,
              )),
          TextSpan(
            text: 'online hours',
          ),
        ],
      ),
    );
  }
}
