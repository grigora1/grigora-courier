import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/buttons.dart';
import 'package:courier_grigora/ui_widgets/layouts/layout_with_back.dart';
import 'package:courier_grigora/ui_widgets/misc_widgets.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:courier_grigora/ui_widgets/ui_utils.dart';
import 'package:courier_grigora/views/pickup_in_progress/pickup_in_progress_screen.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';

class CurrentScheduleScreen extends StatelessWidget {
  const CurrentScheduleScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutWithBack(
      title: 'Current Schedule',
      bgColor: kColorBg,
      actions: [
        Icon(
          EvaIcons.questionMarkCircleOutline,
          color: kColorPrimary,
          size: 24,
        )
            .onTap(() => UiUtils.goto(kRoutePostmanHelp))
            .paddingRight(kSpacingMedium)
      ],
      body: Column(
        children: [
          _ItemOne(
            title: 'Pickup at',
            description: 'Burger Kings',
          ),
          _ItemOne(
            title: 'Deliver to',
            description: 'Nathaniel A.',
          ),
          _SprintOptions()
        ],
      ),
    );
  }
}

class _ItemOne extends StatelessWidget {
  final String title;
  final String description;
  const _ItemOne({Key? key, required this.title, required this.description})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: kColorWhite,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Icon(
            EvaIcons.archiveOutline,
            size: 24,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GrigoraText(
                title,
                isBoldFont: true,
              ),
              GrigoraText(description)
                  .paddingTop(kSpacingSmall)
                  .visible(description is String),
            ],
          ).paddingLeft(kSpacingSmall).expand()
        ],
      ).paddingAll(kSpacingMedium),
    ).paddingBottom(kSpacingSmall);
  }
}

class _SprintOptions extends StatelessWidget {
  const _SprintOptions({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: kColorWhite,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GrigoraTextTitle(
            'Sprint Options',
            textColor: kColorPrimary,
          ).paddingBottom(kSpacingMedium),
          _ItemTwo('Stop orders after this delivery'),
          _ItemTwo('Extend Schedule'),
          _ItemTwo(
            'Need help',
            onTap: () => UiUtils.goto(kRoutePostmanHelp),
          ),
        ],
      ).paddingAll(kSpacingMedium),
    );
  }
}

class _ItemTwo extends StatelessWidget {
  final String text;
  final Function()? onTap;
  const _ItemTwo(this.text, {Key? key, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: kColorBlack.withOpacity(0.05),
      child: Row(
        children: [
          Icon(
            EvaIcons.clockOutline,
            size: 24,
          ),
          GrigoraText(text).paddingLeft(kSpacingSmall).expand(),
          Icon(
            EvaIcons.chevronRight,
            size: 24,
          ).visible(onTap is Function()),
        ],
      ).paddingAll(kSpacingSmall).onTap(onTap),
    ).paddingBottom(kSpacingSmall);
  }
}
