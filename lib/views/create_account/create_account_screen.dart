import 'package:courier_grigora/ui_widgets/faq.dart';
import 'package:courier_grigora/ui_widgets/grigora_horizontal_scroll.dart';
import 'package:courier_grigora/ui_widgets/ui_utils.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/buttons.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:courier_grigora/utils/base_widget.dart';
import 'package:courier_grigora/utils/form_item.dart';
import 'package:courier_grigora/utils/validators.dart';
import 'package:courier_grigora/views/create_account/create_account_vm.dart';
import 'package:provider/provider.dart';
import 'package:nb_utils/nb_utils.dart';

class CreateAccountScreen extends StatelessWidget {
  static ScrollController _scrollController = ScrollController();
  const CreateAccountScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
    return BaseWidget<CreateAccountVM>(
        model: CreateAccountVM(_formKey),
        builder: (context, model, child) {
          return GestureDetector(
            onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
            child: Scaffold(
              appBar: AppBar(
                iconTheme: IconThemeData(color: kColorBlack),
                backgroundColor: kColorPrimaryLight,
                elevation: 0,
                automaticallyImplyLeading: false,
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                        decoration: BoxDecoration(
                            color: kColorWhite,
                            borderRadius: kFullBorderRadius),
                        child: Icon(EvaIcons.menu).paddingAll(kSpacingSmall)),
                    GrigoraText(
                      'Log In',
                      isBoldFont: true,
                      textColor: kColorPrimary,
                    )
                        .paddingAll(kSpacingMedium)
                        .onTap(() => UiUtils.goto(kRouteLogin))
                  ],
                ),
                actions: [],
              ),
              body: Stack(
                children: [
                  SingleChildScrollView(
                      controller: _scrollController,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          _IntroWidget(),
                          _WhatIsGrigora(),
                          _WhyDeliverWithGrigora(),
                          _SignUpDetails(),
                          _HowToDash(),
                          _FAQWidget(),
                          _FooterWidget()
                        ],
                      )),
                  Positioned.fill(
                      child: Visibility(
                    visible: model.isLoading,
                    child: Container(
                        color: kColorWhite.withOpacity(0.8),
                        width: kWidthFull(context),
                        child: Center(child: CircularProgressIndicator())),
                  ))
                ],
              ),
            ),
          );
        });
  }
}

class _FooterLinks extends StatelessWidget {
  final String title;
  final List<String> items;
  const _FooterLinks({Key? key, required this.title, required this.items})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GrigoraTextTitle(
          title,
          textSize: kFontSizeMedium,
          textColor: kColorWhite,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: UiUtils.grigoraDynamicWidget(
              items: items,
              builder: (position) => GrigoraText(
                    items[position],
                    textColor: kColorWhite,
                  ).paddingBottom(kSpacingSmall)),
        ).paddingTop(kSpacingMedium)
      ],
    );
  }
}

class _FooterWidget extends StatelessWidget {
  const _FooterWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: kWidthFull(context),
      color: kColorBlack,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _FooterLinks(
            title: 'Get To Know Us',
            items: [
              'About Us',
              'Careers',
              'Blog',
              'DoorDash Stories',
              'LinkedIn',
              'Glassdoor'
            ],
          ),
          _FooterLinks(
            title: 'Let Us Help You',
            items: [
              'Account Details',
              'Order History',
              'Help',
            ],
          ).paddingTop(kSpacingLarge),
          _FooterLinks(
            title: 'Get the App',
            items: [
              'App Store',
              'Google Play',
            ],
          ).paddingTop(kSpacingLarge),
          _FooterLinks(
            title: 'Doing Business',
            items: [
              'Become a Dasher',
              'Be A Partner Restaurant',
              'Get Dashers For Deliveries',
              'Driving Opportunities',
            ],
          ).paddingTop(kSpacingLarge),
        ],
      ).paddingSymmetric(horizontal: kSpacingMedium, vertical: kSpacingLarge),
    );
  }
}

class _PageTitle extends StatelessWidget {
  final String title;
  const _PageTitle(this.title, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GrigoraTextTitle(
      title,
      isCenter: true,
      textSize: 30,
    );
  }
}

class _FAQWidget extends StatelessWidget {
  const _FAQWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<dynamic> _items = [
      {
        "title": "How does delivering with Grigora work?",
        "subtitle":
            "Millions of people order food through Grigora. When customers place an order, we offer the deliveries to Dashers, who ern money by picking up and delivering them."
      },
      {
        "title": "Where is Grigora available?",
        "subtitle":
            "Millions of people order food through Grigora. When customers place an order, we offer the deliveries to Dashers, who ern money by picking up and delivering them."
      },
      {
        "title": "How long does it take to start working with Grigora?",
        "subtitle":
            "Millions of people order food through Grigora. When customers place an order, we offer the deliveries to Dashers, who ern money by picking up and delivering them."
      },
      {
        "title": "What materials do I need to be a Grigora driver?",
        "subtitle":
            "Millions of people order food through Grigora. When customers place an order, we offer the deliveries to Dashers, who ern money by picking up and delivering them."
      },
      {
        "title": "Are there car requirements for driving with Grigora?",
        "subtitle":
            "Millions of people order food through Grigora. When customers place an order, we offer the deliveries to Dashers, who ern money by picking up and delivering them."
      },
      {
        "title": "Why should I be a Grigora driver?",
        "subtitle":
            "Millions of people order food through Grigora. When customers place an order, we offer the deliveries to Dashers, who ern money by picking up and delivering them."
      },
    ];

    return Container(
      width: kWidthFull(context),
      color: kColorPrimaryLight,
      child: Column(
        children: [
          _PageTitle('FAQ').paddingTop(kSpacingMedium),
          Column(
            children: UiUtils.grigoraDynamicWidget(
                items: _items,
                builder: (position) => FaqItemWidget(
                      title: _items[position]['title'],
                      subtitle: _items[position]['subtitle'],
                    ).paddingBottom(kSpacingSmall)),
          ).paddingTop(kSpacingMedium)
        ],
      ).paddingAll(kSpacingMedium),
    );
  }
}

class _IntroWidget extends StatelessWidget {
  const _IntroWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: kColorPrimaryLight,
      child: Column(
        children: [
          _PageTitle("Your time. Your goals. You're the boss.")
              .paddingTop(kSpacingMedium),
          Container(
            decoration: BoxDecoration(
              color: kColorWhite,
              borderRadius: kBorderRadius,
            ),
            child: Column(
              children: [
                _Form().paddingTop(kSpacingSmall),
              ],
            ).paddingAll(kSpacingMedium),
          ).paddingSymmetric(
              horizontal: kSpacingMedium, vertical: kSpacingLarge),
          Container(
            height: 25,
          )
        ],
      ),
    );
  }
}

class _WhatIsGrigora extends StatelessWidget {
  const _WhatIsGrigora({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: kColorGreyLight,
      child: Column(
        children: [
          _PageTitle('What is Grigora'),
          GrigoraText(
            'Available in over 4,000 cities in the U.S., Canada, and Austraila, Grigora is about connecting people with possibilities: bigger savings accounts, wilder nets, stronger communities, and happier days. We empower local businesses and local drivers (called Dashers) with opportunities to earn, work and live.',
            textAlign: TextAlign.center,
          ).paddingTop(kSpacingMedium),
          GrigoraText(
            'Asa Dasher, you can be your own boss and enjoy the flexibility of choosing when, where, and how much you ern. All you need is a mode of transportation and a smartphone to start making mone. Its that simple.',
            textAlign: TextAlign.center,
          ).paddingTop(kSpacingSmall),
        ],
      ).paddingAll(kSpacingLarge),
    );
  }
}

class _WhyModel extends HorizontalScrollModel {
  final String title, text;
  _WhyModel({required this.title, required this.text});
}

class _WhyDeliverWithGrigora extends StatelessWidget {
  const _WhyDeliverWithGrigora({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<_WhyModel> _items = [
      _WhyModel(
          title: "Know how much you'll make",
          text:
              'Clear and coincide pay model lets youknow how much you will make before accepting any order.'),
      _WhyModel(
          title: "Know how much you'll make",
          text:
              'Clear and coincide pay model lets youknow how much you will make before accepting any order.'),
    ];
    return Column(
      children: [
        _PageTitle('Why deliver with Grigora').paddingAll(kSpacingLarge),
        GrigoraHorizontalScroll<_WhyModel>(
                builder: (position, model) {
                  return Container(
                    width: kWidthFull(context) * 0.6,
                    decoration: BoxDecoration(
                        borderRadius: kBorderRadius,
                        border: Border.all(color: kColorGrey)),
                    child: Column(
                      children: [
                        GrigoraTextTitle(
                          model.title,
                          isCenter: true,
                        ).paddingBottom(kSpacingMedium),
                        GrigoraText(
                          model.text,
                          textAlign: TextAlign.center,
                        )
                      ],
                    ).paddingAll(kSpacingMedium),
                  ).paddingRight(kSpacingMedium);
                },
                items: _items)
            .paddingBottom(kSpacingLarge)
      ],
    );
  }
}

class _SignUpModel extends HorizontalScrollModel {
  final Widget title, text;
  _SignUpModel({required this.title, required this.text});
}

class _SignUpDetails extends StatelessWidget {
  const _SignUpDetails({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<_SignUpModel> _items = [
      _SignUpModel(
          title: Row(
            children: [GrigoraTextTitle('Requirements')],
          ),
          text: UL(
            symbolType: SymbolType.Bullet,
            children: [
              GrigoraText('18 or older'),
              GrigoraText('Any car, scooter, or bicycle (in select cities)'),
              GrigoraText('Driver\'s license number'),
              GrigoraText('Social security number (only in United States)'),
              GrigoraText('Final Step: consent to a background check'),
            ],
          )),
      _SignUpModel(
          title: Row(
            children: [GrigoraTextTitle('How to sign up')],
          ),
          text: UL(
            symbolType: SymbolType.Bullet,
            children: [
              GrigoraText('Submit application'),
              GrigoraText('Choose driver orientation'),
              GrigoraText('Complete sign up'),
              GrigoraText('Get the app and go'),
            ],
          )),
    ];
    return Container(
      color: kColorGreyLight,
      child: Column(
        children: [
          _PageTitle('Sign up details').paddingAll(kSpacingLarge),
          GrigoraHorizontalScroll<_SignUpModel>(
                  builder: (position, model) {
                    return Container(
                      width: kWidthFull(context) * 0.7,
                      decoration: BoxDecoration(
                          color: kColorWhite,
                          borderRadius: kBorderRadius,
                          boxShadow: [kBoxShadow(kColorGreyLight)],
                          border: Border.all(color: kColorGrey)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          model.title.paddingBottom(kSpacingMedium),
                          model.text
                        ],
                      ).paddingAll(kSpacingMedium),
                    ).paddingRight(kSpacingMedium);
                  },
                  items: _items)
              .paddingBottom(kSpacingLarge)
        ],
      ),
    );
  }
}

class _HowToDashModel extends HorizontalScrollModel {
  final String title, text, image;
  _HowToDashModel(
      {required this.title, required this.image, required this.text});
}

class _HowToDash extends StatelessWidget {
  const _HowToDash({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<_HowToDashModel> _items = [
      _HowToDashModel(
          title: "Accept orders",
          image: kImagesHowToDashScreenshot,
          text:
              "We'll automatically send you orders with the full distance and guaranteed pay. Just tap 'Accept' and it's yours."),
      _HowToDashModel(
          title: "Pickup and dropoff",
          image: kImagesHowToDashScreenshot,
          text:
              'In-app instructions will guide you from picking up the order to delivering it to the customer..'),
      _HowToDashModel(
          title: "Get paid",
          image: kImagesHowToDashScreenshot,
          text:
              'We\'ll deposi money into your bank account each week. Track in the "EarningS" tab in your app.'),
    ];
    return Column(
      children: [
        _PageTitle('How to Dash').paddingAll(kSpacingLarge),
        GrigoraHorizontalScroll<_HowToDashModel>(
                builder: (position, model) {
                  return Container(
                    width: kWidthFull(context),
                    child: Column(
                      children: [
                        GrigoraTextTitle(
                          model.title,
                          textSize: 24,
                          isCenter: true,
                        ).paddingBottom(kSpacingSmall),
                        GrigoraText(
                          model.text,
                          textAlign: TextAlign.center,
                        ),
                        Image.asset(model.image)
                      ],
                    ).paddingAll(kSpacingMedium),
                  ).paddingRight(kSpacingMedium);
                },
                items: _items)
            .paddingBottom(kSpacingLarge)
      ],
    );
  }
}

class _Form extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final model = Provider.of<CreateAccountVM>(context);
    return Form(
      key: model.formKey,
      child: Column(
        children: <Widget>[
          Visibility(visible: !model.showStep2, child: _FormStep1()),
          Visibility(visible: model.showStep2, child: _FormStep2()),
          GrigoraTextTitle(
            'Already started signing up?',
            textColor: kColorPrimary,
          ).paddingTop(kSpacingSmall)
        ],
      ),
    );
  }
}

class _FormStep1 extends StatelessWidget {
  const _FormStep1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final model = Provider.of<CreateAccountVM>(context);
    return Column(
      children: [
        GrigoraTextTitle('Start and stop when you want'),
        FormItem(
          placeholder: 'Email',
          onSaved: (val) => model.email = val!,
          keyboardType: EnumKeyboardTpe.email,
          validator: (val) => Validators.validateEmail(val),
        ).paddingTop(kSpacingSmall),
        GrigoraText(
          "By clicking 'Next', I agree to the Independent Contrctor Agreement and have read the Dasher Privacy Policy.",
          textSize: kFontSizeSmall,
        ).paddingTop(kSpacingSmall),
        GrigoraButton(
          onTap: () => model.showStep2 = true,
          text: 'Next',
          withBg: true,
          borderRadius: kBorderRadius,
        ).paddingTop(kSpacingSmall),
      ],
    );
  }
}

class _FormStep2 extends StatelessWidget {
  const _FormStep2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final model = Provider.of<CreateAccountVM>(context);
    return Column(
      children: [
        GrigoraTextTitle('Finish signing up now'),
        Row(
          children: [
            FormItem(
              placeholder: '+1 (US)',
              onSaved: (val) => model.email = val!,
              keyboardType: EnumKeyboardTpe.email,
              validator: (val) => Validators.validateEmail(val),
            ).paddingRight(5).expand(),
            FormItem(
              placeholder: 'Phone',
              onSaved: (val) => model.email = val!,
              keyboardType: EnumKeyboardTpe.email,
              validator: (val) => Validators.validateEmail(val),
            ).paddingLeft(5).expand(),
          ],
        ).paddingTop(kSpacingSmall),
        FormItem(
          placeholder: 'Postal Code',
          onSaved: (val) => model.email = val!,
          keyboardType: EnumKeyboardTpe.email,
          validator: (val) => Validators.validateEmail(val),
        ).paddingTop(kSpacingSmall),
        GrigoraText(
          "By clicking 'Continue', I consent to receive utomated calls/texts from Grigora for informational and/or marketing purposes (consnt for marketing calls us not a condition of any purchase), and acknowlede that I can opt out at any time by replying 'STOP' to any text. Message & Data Rates may apply.",
          textSize: kFontSizeSmall,
        ).paddingTop(kSpacingSmall),
        GrigoraButton(
          onTap: () => UiUtils.goto(kRouteCreateAccount2),
          text: 'Continue',
          withBg: true,
          borderRadius: kBorderRadius,
        ).paddingTop(kSpacingSmall),
      ],
    );
  }
}
