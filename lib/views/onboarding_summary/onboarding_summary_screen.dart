import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/buttons.dart';
import 'package:courier_grigora/ui_widgets/layouts/simple_layout.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:courier_grigora/ui_widgets/ui_utils.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';

class OnboardingSummaryScreen extends StatelessWidget {
  const OnboardingSummaryScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SimpleLayout(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GrigoraTextTitle(
            "Check that your info is correct and read the disclosure below",
            textSize: 30,
          ),
          _PersonalDetails().paddingTop(kSpacingMedium),
          _FieldValue(
            title: 'Driver\'s License State',
            value: 'CO',
          ).paddingTop(kSpacingMedium),
          _FieldValue(
            title: 'Driver\'s License Number',
            value: '369093337',
          ).paddingTop(kSpacingMedium),
          _FieldValue(
            title: 'Birthdate',
            value: '2003-08-14',
          ).paddingTop(kSpacingMedium),
          _FieldValue(
            title: 'Social Security Number',
            value: 'XXX-XX-5556',
          ).paddingTop(kSpacingMedium),
          _FieldValue(
            title: 'Driver\'s License State',
            value: 'CO',
          ).paddingTop(kSpacingMedium),
          GrigoraText(
              'By clicking "Contine", I acknowledge that I have read and agree to the documents above containing the Background Check DIclosure, Additional Disclosures, and Authorization. I authorize Grigora to order reports about me.'),
          GrigoraButton(
            onTap: () => UiUtils.goto(kRouteFinalSteps),
            text: 'Continue',
            withBg: true,
            borderRadius: kBorderRadius,
          ).paddingTop(kSpacingMedium),
        ],
      ).paddingAll(kSpacingMedium),
    );
  }
}

class _PersonalDetails extends StatelessWidget {
  const _PersonalDetails({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        GrigoraTextTitle('Personal Details'),
        Container(
          decoration:
              BoxDecoration(color: kColorGrey, borderRadius: kFullBorderRadius),
          child: Row(
            children: [
              Icon(EvaIcons.edit),
              GrigoraText('Edit').paddingRight(kSpacingSmall)
            ],
          ).paddingSymmetric(horizontal: kSpacingSmall, vertical: 5),
        )
      ],
    );
  }
}

class _FieldValue extends StatelessWidget {
  final String title, value;
  const _FieldValue({Key? key, required this.title, required this.value})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [GrigoraTextTitle(title), GrigoraText(value)],
    );
  }
}
