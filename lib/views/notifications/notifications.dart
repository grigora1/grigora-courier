import 'package:courier_grigora/ui_widgets/layouts/layout_with_back.dart';
import 'package:courier_grigora/ui_widgets/ui_utils.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:nb_utils/nb_utils.dart';

class NotificationsScreen extends StatelessWidget {
  const NotificationsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutWithBack(
      title: 'Notifications',
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [_Notifications()],
      ).paddingAll(kSpacingMedium),
    );
  }
}

class _Notifications extends StatelessWidget {
  const _Notifications({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _items = [
      _ItemModel(
          title: 'Received a Tip',
          description:
              'You received a tip for your last delivery at ST Grogery place'),
      _ItemModel(
          title: 'Order Cancelled',
          description: 'You just cancelled your order to set regular place'),
      _ItemModel(
          title: 'Order Cancelled',
          description: 'You just cancelled your order to set regular place'),
      _ItemModel(
          title: 'Order Cancelled',
          description: 'You just cancelled your order to set regular place'),
    ];
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: UiUtils.grigoraDynamicWidget(
          items: _items,
          builder: (i) => _Item(
                title: _items[i].title,
                description: _items[i].description,
              ).paddingBottom(kSpacingMedium)),
    );
  }
}

class _ItemModel {
  final String title, description;
  _ItemModel({required this.title, required this.description});
}

class _Item extends StatelessWidget {
  final String title, description;
  const _Item({Key? key, required this.title, required this.description})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Icon(
          EvaIcons.archiveOutline,
          size: 24,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GrigoraText(
              title,
              isBoldFont: true,
            ),
            GrigoraText(description).paddingTop(kSpacingSmall)
          ],
        ).paddingLeft(kSpacingSmall).expand()
      ],
    );
  }
}
