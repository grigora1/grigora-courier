import 'package:courier_grigora/ui_widgets/layouts/layout_with_back.dart';
import 'package:courier_grigora/ui_widgets/ui_utils.dart';
import 'package:flutter/material.dart';
import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:nb_utils/nb_utils.dart';

class MessagesScreen extends StatelessWidget {
  const MessagesScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutWithBack(
      title: 'Messages',
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [_Messages()],
      ).paddingAll(kSpacingMedium),
    );
  }
}

class _Messages extends StatelessWidget {
  const _Messages({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _items = [
      _ItemModel(
          title: 'Abdul', description: 'Hey, where are you', date: '6:30 pm'),
      _ItemModel(
          title: 'Abdul', description: 'Hey, where are you', date: '6:30 pm'),
      _ItemModel(
          title: 'Abdul', description: 'Hey, where are you', date: '6:30 pm'),
      _ItemModel(
          title: 'Abdul', description: 'Hey, where are you', date: '6:30 pm'),
    ];
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: UiUtils.grigoraDynamicWidget(
          items: _items,
          builder: (i) => _Item(
                title: _items[i].title,
                description: _items[i].description,
                date: _items[i].date,
              ).paddingBottom(kSpacingMedium)),
    );
  }
}

class _ItemModel {
  final String title, description, date;
  _ItemModel(
      {required this.title, required this.description, required this.date});
}

class _Item extends StatelessWidget {
  final String title, description, date;
  const _Item(
      {Key? key,
      required this.title,
      required this.description,
      required this.date})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GrigoraText(
              title,
              isBoldFont: true,
            ),
            GrigoraText(description).paddingTop(kSpacingSmall)
          ],
        ).expand(),
        GrigoraText(date)
      ],
    );
  }
}
