import 'package:courier_grigora/ui_widgets/layouts/layout_with_back.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:courier_grigora/constants.dart';

class AcceptanceScreen extends StatelessWidget {
  const AcceptanceScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutWithBack(
      title: 'Acceptance',
      body: Container(
        width: kWidthFull(context),
        child: Column(
          children: [
            GrigoraTextTitle(
              '85%',
              textColor: kColorAccent,
              textSize: 45,
            ),
            GrigoraText(
              'NOT ENOUGH DATA',
              textSize: kFontSizeSmall,
            ).paddingTop(kSpacingMedium),
            GrigoraText(
              'Acceptance rate is the rate of orders you accepted out of the last 100 delivery oppurtunities consistently accept oppurtunities to raise your acceptance rate',
              isCenter: true,
            ).paddingTop(kSpacingMedium)
          ],
        ).paddingAll(kSpacingMedium),
      ),
    );
  }
}
