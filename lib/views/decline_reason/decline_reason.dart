import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/buttons.dart';
import 'package:courier_grigora/ui_widgets/layouts/layout_with_back.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:courier_grigora/ui_widgets/ui_utils.dart';
import 'package:courier_grigora/utils/app_enums.dart';
import 'package:courier_grigora/utils/base_widget.dart';
import 'package:courier_grigora/views/decline_reason/decline_reason_vm.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';

class DeclineReasonScreen extends StatelessWidget {
  const DeclineReasonScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseWidget<DeclineReasonVM>(
        model: DeclineReasonVM(),
        builder: (context, model, child) {
          return LayoutWithBack(
              title: 'Please select a reason',
              bgColor: kColorBg,
              body: Column(
                children: [
                  _Reasons(
                    items: [
                      'Distance is too far',
                      'Store is not in my starting point',
                      'The order is too small',
                      'I don\'t want to place order',
                      'I don\'t want to gp to this store',
                      'I have too many orders',
                      'I have phone or app problems',
                      'I need to use the toilet',
                      'I have an emergency',
                      'Store is closed',
                    ],
                  ),
                  Column(
                    children: [
                      GrigoraButton(
                        onTap: () => UiUtils.goto(kRouteDeclineAccept),
                        text: 'Submit',
                        withBg: true,
                      ).paddingTop(kSpacingMedium),
                      GrigoraButton(
                        onTap: () {
                          UiUtils.goto(kRouteDashboard);
                          model.state = EnumHomeState.orderSearch;
                        },
                        text: 'Cancel',
                      ).paddingTop(kSpacingMedium)
                    ],
                  ).paddingAll(kSpacingMedium)
                ],
              ));
        });
  }
}

class _Reasons extends StatelessWidget {
  final List<String> items;
  const _Reasons({Key? key, required this.items}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: UiUtils.grigoraDynamicWidget(
          items: items,
          builder: (i) => Container(
                  width: kWidthFull(context),
                  color: kColorWhite,
                  child: GrigoraText(items[i]).paddingAll(kSpacingMedium))
              .paddingTop(5)),
    ).paddingTop(kSpacingSmall);
  }
}
