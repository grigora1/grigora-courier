import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/layouts/layout_without_back.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:courier_grigora/ui_widgets/ui_utils.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';

class PerformanceScreen extends StatelessWidget {
  const PerformanceScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutWithoutBack(
      title: 'Performance',
      body: Container(
        width: kWidthFull(context),
        child: Column(
          // crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GrigoraTextTitle(
              'Customer Rating',
              isCenter: true,
            ),
            GrigoraTextTitle(
              '4.32',
              textColor: kColorAccent,
              textSize: 45,
            ).paddingTop(kSpacingSmall),
            GrigoraText('39 sprints, 32.5 hours'),
            _Col2Table(),
            _HorizontalDivider(),
            _Col3Table(),
            _HorizontalDivider(),
            Container(
              width: kWidthFull(context),
              child: GrigoraText(
                'Based on last 100 deliveries',
                isItalic: true,
                textColor: kColorBlack.withOpacity(0.5),
                textAlign: TextAlign.right,
              ).paddingSymmetric(vertical: kSpacingMedium),
            ),
            _HorizontalDivider(),
            _LearnMore().paddingSymmetric(vertical: kSpacingMedium),
            _HorizontalDivider(),
            _Feedbacks().paddingTop(kSpacingMedium)
          ],
        ).paddingAll(kSpacingMedium).paddingBottom(kSpacingLarge),
      ),
    );
  }
}

class _Feedbacks extends StatelessWidget {
  const _Feedbacks({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GrigoraTextTitle(
          'Feedbacks',
        ),
        _FeedbackItem(
          number: '32',
          text: 'Fast Delivery',
        ).paddingTop(kSpacingMedium),
        _FeedbackItem(
          number: '32',
          text: 'Greate service',
        ).paddingTop(kSpacingSmall),
        _FeedbackItem(
          number: '32',
          text: 'Order in good shape',
        ).paddingTop(kSpacingSmall),
        _FeedbackItem(
          number: '32',
          text: 'Supportive',
        ).paddingTop(kSpacingSmall)
      ],
    );
  }
}

class _FeedbackItem extends StatelessWidget {
  final String text, number;
  const _FeedbackItem({Key? key, required this.text, required this.number})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      Container(
        decoration: BoxDecoration(color: kColorAccent.withOpacity(0.1)),
        child: GrigoraTextTitle(number).paddingAll(kSpacingMedium),
      ),
      GrigoraText(text).paddingLeft(kSpacingMedium)
    ]);
  }
}

class _LearnMore extends StatelessWidget {
  const _LearnMore({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        GrigoraText('Learn how Rating works').expand(),
        Icon(
          EvaIcons.chevronRight,
          size: 32,
        )
      ],
    );
  }
}

class _HorizontalDivider extends StatelessWidget {
  const _HorizontalDivider({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: kWidthFull(context),
      height: 1,
      color: kColorBlack.withOpacity(0.2),
    );
  }
}

class _Col2Table extends StatelessWidget {
  const _Col2Table({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        _Item(title: 'Reliability', value: '72%', color: kColorSecondary)
            .expand(),
        Container(
          height: 70,
          width: 1,
          color: kColorBlack.withOpacity(0.2),
        ),
        _Item(title: 'Success', value: '85%').expand()
      ],
    );
  }
}

class _Col3Table extends StatelessWidget {
  const _Col3Table({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        _Item(title: 'Acceptance', value: '72%', color: kColorSecondary)
            .onTap(() => UiUtils.goto(kRouteAcceptance))
            .expand(),
        Container(
          height: 70,
          width: 1,
          color: kColorBlack.withOpacity(0.2),
        ),
        _Item(title: 'Completion', value: '85%').expand(),
        Container(
          height: 70,
          width: 1,
          color: kColorBlack.withOpacity(0.2),
        ),
        _Item(title: 'On-time', value: '85%').expand()
      ],
    );
  }
}

class _Item extends StatelessWidget {
  final String title, value;
  final Color? color;
  const _Item({Key? key, required this.title, this.color, required this.value})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GrigoraText(
          title,
          isBoldFont: true,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          isCenter: true,
        ),
        GrigoraTextTitle(
          value,
          textColor: color ?? kColorAccent,
          textSize: kFontSizeBig,
        ).paddingTop(kSpacingSmall),
      ],
    ).paddingAll(kSpacingMedium);
  }
}
