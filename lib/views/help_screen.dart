import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:nb_utils/nb_utils.dart';

class HelpScreen extends StatelessWidget {
  const HelpScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: kColorBlack),
        backgroundColor: kColorWhite,
        centerTitle: true,
        elevation: 0,
        title: GrigoraTextTitle('Help'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: kPaddingAllMedium,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GrigoraTextTitle('Can we help with an order'),
              _Item(
                text: 'I need help with a current order',
                onTap: () => print('tapping this one'),
              ).paddingTop(kSpacingMedium),
              _Item(
                text: 'I need hep wit a previous order',
                onTap: () => print('tapping this one'),
              ),
              _Item(
                text: 'I\'m having a problem while ordering',
                onTap: () => print('tapping this one'),
              ),
              _Item(
                text: 'I have a partner question',
                onTap: () => print('tapping this one'),
              ),
              _Item(
                text: 'COVID-19 questions',
                onTap: () => print('tapping this one'),
              ),
              _Item(
                text: 'Subscriptions and billing',
                onTap: () => print('tapping this one'),
                hideLine: true,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _Item extends StatelessWidget {
  final bool hideLine;
  final String text;
  final Function() onTap;
  const _Item(
      {Key? key,
      this.hideLine = false,
      required this.text,
      required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: kSpacingMedium),
            child: Row(
              children: [
                Expanded(child: GrigoraText(text)),
                Icon(
                  EvaIcons.chevronRight,
                  color: kColorBlack,
                )
              ],
            ),
          ),
          Visibility(
            visible: !hideLine,
            child: Container(
              color: kColorGrey,
              height: 1,
            ),
          )
        ],
      ),
    );
  }
}
