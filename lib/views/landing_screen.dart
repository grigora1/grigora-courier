import 'package:flutter/material.dart';
import 'package:courier_grigora/constants.dart';

class LandingWidget extends StatefulWidget {
  @override
  _LandingWidgetState createState() => _LandingWidgetState();
}

class _LandingWidgetState extends State<LandingWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Image.asset(
          kImagesGrigoraLogo,
          width: 200,
        ),
      ),
    );
  }
}
