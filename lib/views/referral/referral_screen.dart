import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:courier_grigora/ui_widgets/ui_utils.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';

class ReferralScreen extends StatelessWidget {
  const ReferralScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: kWidthFull(context),
        height: kHeightFull(context),
        color: kColorAccent,
        child: SafeArea(
          child: Stack(
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GrigoraTextTitle(
                    'Give N50, Get N300!',
                    textSize: kFontSizeBig,
                    textColor: kColorWhite,
                    isCenter: true,
                  ),
                  GrigoraText(
                    'Get N300 in credits when someone signs up using your referral link and places their first order over N2000. Your friend also fets N1000 off (N1000 off ech of their first 2 orders.)',
                    textColor: kColorWhite,
                    isCenter: true,
                  ).paddingTop(kSpacingMedium),
                  GrigoraText(
                    'SHARE YOUR LINK',
                    textColor: kColorWhite,
                    isBoldFont: true,
                  ).paddingTop(kSpacingLarge),
                  _CopyLinkWidget().paddingTop(kSpacingSmall)
                ],
              ).paddingAll(kSpacingMedium),
              Positioned(
                  right: 10,
                  top: 10,
                  child: _BackButtonWidget().onTap(() => UiUtils.goBack()))
            ],
          ),
        ),
      ),
    );
  }
}

class _CopyLinkWidget extends StatelessWidget {
  const _CopyLinkWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 320,
      decoration: BoxDecoration(
          color: kColorWhite,
          borderRadius: kBorderRadiusSmall,
          boxShadow: [kBoxShadow(kColorBlack.withOpacity(0.01))]),
      child: Row(
        children: [
          GrigoraText(
            'rvbkiugdsg77f/Drozzoo/Refer',
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            isBoldFont: true,
          ).expand(),
          Icon(
            EvaIcons.copyOutline,
            color: kColorAccent,
          )
        ],
      ).paddingAll(kSpacingMedium),
    );
  }
}

class _BackButtonWidget extends StatelessWidget {
  const _BackButtonWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration:
          BoxDecoration(color: kColorWhite, borderRadius: kFullBorderRadius),
      child: Icon(
        EvaIcons.close,
        color: kColorAccent,
        size: 32,
      ).paddingAll(kSpacingSmall),
    );
  }
}
