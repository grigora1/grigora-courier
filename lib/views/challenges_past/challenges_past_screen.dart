import 'package:courier_grigora/ui_widgets/buttons.dart';
import 'package:courier_grigora/ui_widgets/dashboard_widgets.dart';
import 'package:courier_grigora/ui_widgets/layouts/layout_with_back.dart';
import 'package:courier_grigora/ui_widgets/ui_utils.dart';
import 'package:flutter/material.dart';
import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:nb_utils/nb_utils.dart';

class ChallengePastScreen extends StatelessWidget {
  const ChallengePastScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutWithBack(
      title: 'Past Challenge',
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _Milestone(),
          _OrderDelivered().paddingTop(kSpacingBigMedium),
          _TotalReward().paddingTop(kSpacingMedium),
          HorizontalDivider().paddingTop(kSpacingMedium),
          _Rules().paddingTop(kSpacingMedium),
          HorizontalDivider().paddingTop(kSpacingMedium),
          _Conditions().paddingTop(kSpacingMedium),
          HorizontalDivider().paddingTop(kSpacingMedium),
          _DateTime().paddingTop(kSpacingMedium),
          GrigoraButton(
            onTap: () => UiUtils.goto(kRouteChallenges),
            text: 'Join Challenge',
            withBg: true,
          ).paddingTop(kSpacingMedium),
        ],
      ).paddingAll(kSpacingMedium),
    );
  }
}

class _TotalReward extends StatelessWidget {
  const _TotalReward({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GrigoraTextTitle('Total Reward'),
        GrigoraText(
          'N1,500',
          textColor: kColorAccent,
        ).paddingTop(kSpacingSmall),
      ],
    );
  }
}

class _DateTime extends StatelessWidget {
  const _DateTime({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GrigoraTextTitle('Date & Time'),
        GrigoraText('2 September - 16 September').paddingTop(kSpacingSmall),
        GrigoraText(
          '8am - 10am',
          textColor: kColorPrimary,
        ).paddingTop(kSpacingMedium),
      ],
    );
  }
}

class _Conditions extends StatelessWidget {
  const _Conditions({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GrigoraTextTitle('Conditions'),
        GrigoraText('Completion rate should be \ngreater than 90%')
            .paddingTop(kSpacingSmall),
        RichText(
          text: new TextSpan(
            text: 'YOURS: ',
            style: kBodyText1Style,
            children: <TextSpan>[
              new TextSpan(
                  text: '35%',
                  style: new TextStyle(
                    color: kColorPrimary,
                    decoration: TextDecoration.underline,
                  )),
            ],
          ),
        ).paddingTop(kSpacingMedium),
      ],
    );
  }
}

class _Rules extends StatelessWidget {
  const _Rules({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GrigoraTextTitle('Rules'),
        GrigoraText('Deliver 30 more orders and \nN5000 is all yours!')
            .paddingTop(kSpacingSmall),
      ],
    );
  }
}

class _OrderDelivered extends StatelessWidget {
  const _OrderDelivered({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: kWidthFull(context),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              GrigoraTextTitle(
                '40 ',
                textSize: 40,
                textColor: kColorAccent,
              ),
              GrigoraTextTitle(
                'out 50',
              ),
            ],
          ),
          GrigoraText(
            'Orders delivered',
            textColor: kColorPrimary,
          ).paddingTop(kSpacingSmall)
        ],
      ),
    );
  }
}

class _Milestone extends StatelessWidget {
  const _Milestone({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GrigoraTextTitle(
              'Milestone',
            ),
            GrigoraText('Deliver 30 more orders and \nN5000 is all yours!')
                .paddingTop(kSpacingSmall),
          ],
        ).expand(),
        GrigoraText(
          '4 days left',
          textSize: kFontSizeSmall,
        )
      ],
    );
  }
}
