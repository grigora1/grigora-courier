import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/buttons.dart';
import 'package:courier_grigora/ui_widgets/layouts/layout_with_back.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:courier_grigora/ui_widgets/ui_utils.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';

class PausedSprintScreen extends StatelessWidget {
  const PausedSprintScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutWithBack(
      title: 'Paused',
      actions: [
        Icon(
          EvaIcons.questionMarkCircleOutline,
          color: kColorPrimary,
        )
            .onTap(() => UiUtils.goto(kRoutePostmanHelp))
            .paddingRight(kSpacingMedium)
      ],
      body: Container(
        height: kHeightFull(context) - 150,
        child: Column(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    _TimeWidget(
                      value: '30',
                      dimension: 'Mins',
                    ),
                    GrigoraTextTitle(
                      ':',
                    ).paddingSymmetric(horizontal: kSpacingMedium),
                    _TimeWidget(
                      value: '45',
                      dimension: 'Secs',
                    ),
                  ],
                ),
                GrigoraText(
                  'We won\'t send you orders while you\'re paused',
                  isBoldFont: true,
                  isCenter: true,
                ).paddingTop(kSpacingLarge),
                GrigoraText(
                  'Pausing for more than 35 minutes will end your sprint',
                  isBoldFont: true,
                  isCenter: true,
                ).paddingTop(kSpacingMedium),
              ],
            ).paddingSymmetric(horizontal: kSpacingMedium).expand(),
            GrigoraButton(
              onTap: () => UiUtils.goto(kRouteDashboard),
              text: 'Resume Sprint',
              withBg: true,
            ),
            GrigoraButton(
              onTap: () => UiUtils.goto(kRouteDashboard),
              text: 'End Sprint',
            ).paddingTop(kSpacingSmall)
          ],
        ).paddingAll(kSpacingMedium),
      ),
    );
  }
}

class _TimeWidget extends StatelessWidget {
  final String value, dimension;
  const _TimeWidget({Key? key, required this.value, required this.dimension})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: kColorPrimary.withOpacity(0.1), borderRadius: kBorderRadius),
      child: Column(
        children: [
          GrigoraTextTitle(
            value,
            textSize: 35,
          ),
          GrigoraText(dimension)
        ],
      ).paddingAll(kSpacingMedium),
    );
  }
}
