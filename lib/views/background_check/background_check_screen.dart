import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/buttons.dart';
import 'package:courier_grigora/ui_widgets/layouts/simple_layout.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:courier_grigora/ui_widgets/ui_utils.dart';
import 'package:courier_grigora/utils/form_item.dart';
import 'package:courier_grigora/utils/validators.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';

class BackgroundCheckScreen extends StatelessWidget {
  const BackgroundCheckScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SimpleLayout(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GrigoraTextTitle(
            "Background Check",
            textSize: 30,
          ),
          GrigoraText(
                  "This is a standard procedure and should take no more than 1 to 3 business days.")
              .paddingTop(kSpacingMedium),
          _Form()
        ],
      ).paddingAll(kSpacingMedium),
    );
  }
}

class _Form extends StatelessWidget {
  const _Form({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Form(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          FormItem(
            placeholder: 'Driver\'s Licence State',
            // onSaved: (val) => model.email = val!,
            withNoBorder: true,
            keyboardType: EnumKeyboardTpe.email,
            validator: (val) => Validators.validateEmail(val),
          ).paddingTop(kSpacingSmall),
          Divider(
            color: kColorDarkGrey,
          ),
          FormItem(
            placeholder: 'Driver\'s Licence Number',
            // onSaved: (val) => model.email = val!,
            withNoBorder: true,
            keyboardType: EnumKeyboardTpe.email,
            validator: (val) => Validators.validateEmail(val),
          ).paddingTop(kSpacingSmall),
          Divider(
            color: kColorDarkGrey,
          ),
          FormItem(
            placeholder: 'Social Security Number',
            // onSaved: (val) => model.password = val!,
            withNoBorder: true,
            validator: (val) => Validators.validateText(val),
          ),
          Divider(
            color: kColorDarkGrey,
          ),
          FormItem(
            placeholder: 'Confirm Social Security Number',
            // onSaved: (val) => model.password = val!,
            withNoBorder: true,
            validator: (val) => Validators.validateText(val),
          ),
          Divider(
            color: kColorDarkGrey,
          ),
          GrigoraButton(
            onTap: () => UiUtils.goto(kRouteOnboardingSummary),
            text: 'Continue',
            withBg: true,
            borderRadius: kBorderRadius,
          ).paddingTop(kSpacingMedium),
        ],
      ),
    );
  }
}
