import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/buttons.dart';
import 'package:courier_grigora/ui_widgets/layouts/layout_with_back.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:courier_grigora/ui_widgets/ui_utils.dart';
import 'package:courier_grigora/utils/form_item.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';

class ScheduleChangeScreen extends StatelessWidget {
  const ScheduleChangeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutWithBack(
      title: 'Change Schedule',
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GrigoraTextTitle('January 5, 2020'),
          _AddressWidget().paddingTop(kSpacingSmall),
          FormItem(
            placeholder: '7:30 AM',
            label: 'Start Time',
          ).paddingTop(kSpacingMedium),
          FormItem(
            placeholder: '9:00 PM',
            label: 'End Time',
          ).paddingTop(kSpacingMedium),
          // Container().expand(),
          GrigoraButton(
            onTap: () => print('save'),
            text: 'Save',
            withBg: true,
          ).paddingTop(kSpacingExtraLarge),

          GrigoraButton(
            onTap: () => UiUtils.goto(kRouteScheduleDelete),
            text: 'Delete Schedule',
          ).paddingTop(kSpacingMedium),
        ],
      ).paddingAll(kSpacingMedium),
    );
  }
}

class _AddressWidget extends StatelessWidget {
  const _AddressWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(
          EvaIcons.carOutline,
          color: kColorAccent,
        ),
        GrigoraText('Ilorin West Avenue').paddingLeft(kSpacingSmall)
      ],
    );
  }
}
