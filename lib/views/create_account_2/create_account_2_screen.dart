import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/buttons.dart';
import 'package:courier_grigora/ui_widgets/layouts/simple_layout.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:courier_grigora/ui_widgets/ui_utils.dart';
import 'package:courier_grigora/utils/form_item.dart';
import 'package:courier_grigora/utils/validators.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';

class CreateAccount2Screen extends StatelessWidget {
  const CreateAccount2Screen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SimpleLayout(
      body: Column(
        children: [
          GrigoraTextTitle(
            "You're in! Let's get started",
            textSize: 30,
          ),
          GrigoraText(
                  "Start setting up your profile. Make sure you have your driver's license (if you plan to drive) and your Social Security Number handy.")
              .paddingTop(kSpacingMedium),
          _Form()
        ],
      ).paddingAll(kSpacingMedium),
    );
  }
}

class _Form extends StatelessWidget {
  const _Form({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Form(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          FormItem(
            placeholder: 'Legal First Nmae',
            // onSaved: (val) => model.email = val!,
            withNoBorder: true,
            keyboardType: EnumKeyboardTpe.email,
            validator: (val) => Validators.validateEmail(val),
          ).paddingTop(kSpacingSmall),
          Divider(
            color: kColorDarkGrey,
          ),
          FormItem(
            placeholder: 'Legal Middle Name',
            // onSaved: (val) => model.email = val!,
            withNoBorder: true,
            keyboardType: EnumKeyboardTpe.email,
            validator: (val) => Validators.validateEmail(val),
          ).paddingTop(kSpacingSmall),
          Divider(
            color: kColorDarkGrey,
          ),
          GrigoraTextTitle(
            '(Optional) Enter referral details',
            textColor: kColorPrimary,
          ).paddingTop(kSpacingSmall),
          Divider(
            color: kColorDarkGrey,
          ).paddingSymmetric(vertical: kSpacingLarge),
          GrigoraTextTitle(
            'Create your account',
          ).paddingTop(kSpacingSmall),
          FormItem(
            placeholder: 'Password',
            // onSaved: (val) => model.password = val!,
            withNoBorder: true,
            password: true,
            validator: (val) => Validators.validateText(val),
          ),
          Divider(
            color: kColorDarkGrey,
          ),
          FormItem(
            placeholder: 'Confirm Password',
            // onSaved: (val) => model.password = val!,
            withNoBorder: true,
            password: true,
            validator: (val) => Validators.validateText(val),
          ),
          Divider(
            color: kColorDarkGrey,
          ),
          GrigoraButton(
            onTap: () => UiUtils.goto(kRouteVehicleDetails),
            text: 'Continue',
            withBg: true,
            borderRadius: kBorderRadius,
          ).paddingTop(kSpacingMedium),
        ],
      ),
    );
  }
}
