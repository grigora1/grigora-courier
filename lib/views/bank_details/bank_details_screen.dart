import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/layouts/layout_with_back.dart';
import 'package:courier_grigora/utils/form_item.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';

class BankDetailsScreen extends StatelessWidget {
  const BankDetailsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutWithBack(
      title: 'Bank Details',
      body: Form(
        child: Stack(
          children: [
            Column(
              children: [
                FormItem(
                  label: 'Bank',
                  placeholder: 'First Bank',
                ),
                FormItem(
                  label: 'Account number',
                  placeholder: '1234567890',
                ).paddingTop(kSpacingMedium),
                FormItem(
                  label: 'Account Name',
                  placeholder: 'Nathaniel',
                ).paddingTop(kSpacingMedium),
                RichText(
                  text: new TextSpan(
                    text: 'To edit your bank details ',
                    style: kBodyText1Style,
                    children: <TextSpan>[
                      new TextSpan(
                          text: 'contact support',
                          style: new TextStyle(
                            color: kColorPrimary,
                            decoration: TextDecoration.underline,
                          )),
                    ],
                  ),
                ).paddingTop(kSpacingExtraLarge),
              ],
            ),
          ],
        ),
      ).paddingAll(kSpacingMedium),
    );
  }
}
