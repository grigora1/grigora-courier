import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/layouts/layout_with_back.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';

class VehicleInformationScreen extends StatelessWidget {
  const VehicleInformationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutWithBack(
      title: 'Vehicle Information',
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GrigoraText('VEHICLES'),
          _Items().paddingTop(kSpacingMedium)
        ],
      ).paddingAll(kSpacingMedium),
    );
  }
}

class _Items extends StatelessWidget {
  const _Items({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _Item(
          title: 'Vehicle 1',
          subtitle: 'LN: 9357GGYU09',
        ),
        _Item(
          title: 'Vehicle 2',
          subtitle: 'LN: 9357GGYU09',
        ),
      ],
    );
  }
}

class _Item extends StatelessWidget {
  final String title, subtitle;
  const _Item({Key? key, required this.title, required this.subtitle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Container(
              width: 70,
              height: 70,
              decoration: BoxDecoration(
                  color: kColorBlack.withOpacity(0.2),
                  borderRadius: kFullBorderRadius),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [GrigoraTextTitle(title), GrigoraText(subtitle)],
            ).paddingLeft(kSpacingSmall),
          ],
        ),
        Divider()
      ],
    );
  }
}
