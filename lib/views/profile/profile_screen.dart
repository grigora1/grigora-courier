import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/layouts/layout_with_back.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:courier_grigora/utils/form_item.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutWithBack(
      title: 'Profile',
      body: Form(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            FormItem(
              label: 'Full name',
              placeholder: 'Nathan Abdul',
            ),
            FormItem(
              label: 'Username',
              placeholder: 'nate247',
            ).paddingTop(kSpacingMedium),
            FormItem(
              label: 'Email',
              placeholder: 'natedesign@email.com',
            ).paddingTop(kSpacingMedium),
            _CountrySelectWidget().paddingTop(kSpacingMedium),
            RichText(
              text: new TextSpan(
                text: 'To edit your profile details ',
                style: kBodyText1Style,
                children: <TextSpan>[
                  new TextSpan(
                      text: 'contact support',
                      style: new TextStyle(
                        color: kColorPrimary,
                        decoration: TextDecoration.underline,
                      )),
                ],
              ),
            ).center().paddingTop(kSpacingExtraLarge),
          ],
        ).paddingAll(kSpacingMedium),
      ),
    );
  }
}

class _CountrySelectWidget extends StatelessWidget {
  const _CountrySelectWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GrigoraText(
          'Phone Number',
          isBoldFont: true,
        ),
        Row(
          children: [
            _CountrySelect().paddingRight(kSpacingSmall),
            FormItem(
              // label: 'Phone Number',
              placeholder: '+44 8549 8398 34985',
            ).expand(),
          ],
        ).paddingTop(kSpacingSmall),
      ],
    );
  }
}

class _CountrySelect extends StatelessWidget {
  const _CountrySelect({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: kBorderRadius,
          border: Border.all(color: kColorBlack.withOpacity(0.4))),
      child: Row(
        children: [Icon(EvaIcons.chevronDown)],
      ).paddingAll(kSpacingMedium),
    );
  }
}
