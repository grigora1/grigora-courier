import 'package:flutter/material.dart';
import 'package:courier_grigora/ui_widgets/layouts/layout_with_reliability.dart';

class ScheduleDeleteScreen extends StatelessWidget {
  const ScheduleDeleteScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutWithReliability(
      title: 'Are you sure you want to delete this schedule?',
      description: 'You\'re commited to working for this period.',
      reliabilityScore: '72%',
      isReliability: true,
      buttonText: 'Yes, delete',
    );
  }
}
