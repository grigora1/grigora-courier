import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/utils/app_enums.dart';
import 'package:courier_grigora/utils/base_widget.dart';
import 'package:courier_grigora/views/accounts/accounts_screen.dart';
import 'package:courier_grigora/views/dashboard/dashboard_vm.dart';
import 'package:courier_grigora/views/earnings/earnings_screen.dart';
import 'package:courier_grigora/views/home/home_screen.dart';
import 'package:courier_grigora/views/performance/performance_screen.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:courier_grigora/views/schedule/schedule_screen.dart';

class DashboardScreen extends StatefulWidget {
  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  int _selectedIndex = 0;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<DashboardVM>(
        model: DashboardVM(),
        builder: (context, model, child) {
          List<_DashboardItemModel> _bottomSheetItems = [
            _DashboardItemModel(
                widget: HomeScreen(),
                appBar: model.state == EnumHomeState.orderSearch
                    ? homeAppBar()
                    : null,
                barItem: BottomNavigationBarItem(
                  icon: Icon(EvaIcons.navigation2Outline),
                  label: 'Home',
                )),
            _DashboardItemModel(
                widget: ScheduleScreen(),
                barItem: BottomNavigationBarItem(
                  icon: Icon(EvaIcons.calendarOutline),
                  label: 'Schedule',
                )),
            _DashboardItemModel(
                widget: EarningsScreen(),
                barItem: BottomNavigationBarItem(
                  icon: Icon(EvaIcons.pricetagsOutline),
                  label: 'Earnings',
                )),
            _DashboardItemModel(
                widget: PerformanceScreen(),
                barItem: BottomNavigationBarItem(
                  icon: Icon(EvaIcons.barChart),
                  label: 'Performance',
                )),
            _DashboardItemModel(
                widget: AccountScreen(),
                barItem: BottomNavigationBarItem(
                  icon: Icon(EvaIcons.personOutline),
                  label: 'Accounts',
                )),
          ];

          return Scaffold(
              appBar: _bottomSheetItems[_selectedIndex].appBar,
              body: _bottomSheetItems[_selectedIndex].widget,
              bottomNavigationBar: BottomNavigationBar(
                type: BottomNavigationBarType.fixed,
                items: _bottomSheetItems
                    .map((_DashboardItemModel e) => e.barItem)
                    .toList(),
                currentIndex: _selectedIndex,
                selectedItemColor: kColorPrimary,
                onTap: _onItemTapped,
              ));
        });
  }
}

class _DashboardItemModel {
  final Widget widget;
  final PreferredSizeWidget? appBar;
  final BottomNavigationBarItem barItem;

  _DashboardItemModel(
      {required this.widget, this.appBar, required this.barItem});
}
