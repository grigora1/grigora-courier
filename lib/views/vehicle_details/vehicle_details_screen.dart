import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/buttons.dart';
import 'package:courier_grigora/ui_widgets/layouts/simple_layout.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:courier_grigora/ui_widgets/ui_utils.dart';
import 'package:courier_grigora/utils/form_item.dart';
import 'package:courier_grigora/utils/validators.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';

class VehicleDetailsScreen extends StatelessWidget {
  const VehicleDetailsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SimpleLayout(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GrigoraTextTitle(
            "Vehicle Details",
            textSize: 30,
          ),
          GrigoraText(
                  "Enter your vehicle details and additional personal information")
              .paddingTop(kSpacingMedium),
          _Form()
        ],
      ).paddingAll(kSpacingMedium),
    );
  }
}

class _Form extends StatelessWidget {
  const _Form({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Form(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          FormItem(
            placeholder: 'Vehicle Type',
            // onSaved: (val) => model.email = val!,
            withNoBorder: true,
            keyboardType: EnumKeyboardTpe.email,
            validator: (val) => Validators.validateEmail(val),
          ).paddingTop(kSpacingSmall),
          Divider(
            color: kColorDarkGrey,
          ).paddingTop(kSpacingSmall),
          FormItem(
            placeholder: 'Make',
            // onSaved: (val) => model.email = val!,
            withNoBorder: true,
            keyboardType: EnumKeyboardTpe.email,
            validator: (val) => Validators.validateEmail(val),
          ).paddingTop(kSpacingSmall),
          Divider(
            color: kColorDarkGrey,
          ).paddingTop(kSpacingSmall),
          FormItem(
            placeholder: 'Model',
            // onSaved: (val) => model.email = val!,
            withNoBorder: true,
            keyboardType: EnumKeyboardTpe.email,
            validator: (val) => Validators.validateEmail(val),
          ).paddingTop(kSpacingSmall),
          Divider(
            color: kColorDarkGrey,
          ).paddingTop(kSpacingSmall),
          FormItem(
            placeholder: 'Color',
            // onSaved: (val) => model.email = val!,
            withNoBorder: true,
            keyboardType: EnumKeyboardTpe.email,
            validator: (val) => Validators.validateEmail(val),
          ).paddingTop(kSpacingSmall),
          Divider(
            color: kColorDarkGrey,
          ).paddingTop(kSpacingSmall),
          GrigoraButton(
            onTap: () => UiUtils.goto(kRouteBackgroundCheck),
            text: 'Continue',
            withBg: true,
            borderRadius: kBorderRadius,
          ).paddingTop(kSpacingMedium),
        ],
      ),
    );
  }
}
