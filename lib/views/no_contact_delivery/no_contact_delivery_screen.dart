import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/buttons.dart';
import 'package:courier_grigora/ui_widgets/layouts/layout_with_back.dart';
import 'package:courier_grigora/ui_widgets/misc_widgets.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:courier_grigora/ui_widgets/ui_utils.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';

class NoContactDeliveryScreen extends StatelessWidget {
  const NoContactDeliveryScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutWithBack(
      bgColor: kColorBg,
      elevation: 5,
      title: 'No contact delivery',
      body: Column(
        children: [
          _DeliveryInstruction(),
          _TakeAPhoto().paddingTop(kSpacingSmall),
          GrigoraButton(
            onTap: () => UiUtils.showDialog(_SubmitAlertWidget()),
            text: 'Submit',
            withBg: true,
          ).paddingAll(kSpacingMedium)
        ],
      ),
    );
  }
}

class _SubmitAlertWidget extends StatelessWidget {
  const _SubmitAlertWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        GrigoraTextTitle(
          'Confirm delivery was complete',
          isCenter: true,
        ),
        GrigoraButton(
          onTap: () => UiUtils.showDialog(_DeliveryCompleteAlertWidget()),
          text: 'Confirm',
          withBg: true,
        ).paddingTop(kSpacingMedium),
        GrigoraButton(
          onTap: () => UiUtils.goBack(),
          text: 'Go back',
        ).paddingTop(kSpacingSmall),
      ],
    );
  }
}

class _DeliveryCompleteAlertWidget extends StatelessWidget {
  const _DeliveryCompleteAlertWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        MiscWidgetDeliveryCompleteItems(),
      ],
    );
  }
}

class _DeliveryInstruction extends StatelessWidget {
  const _DeliveryInstruction({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: kColorWhite,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Icon(
            EvaIcons.messageCircleOutline,
            size: kFontSizeIconSmall,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GrigoraTextTitle('Delivery Instruction'),
              GrigoraText(
                'Contactless delivery',
                textColor: kColorPrimary,
              ),
            ],
          ).paddingLeft(kSpacingSmall).expand(),
          Icon(EvaIcons.chevronDown)
        ],
      ).paddingAll(kSpacingMedium),
    );
  }
}

class _TakeAPhoto extends StatelessWidget {
  const _TakeAPhoto({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: kColorWhite,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GrigoraTextTitle('Take a photo'),
          GrigoraText(
                  'Take a photo of where you left the order. We will shate it with the customer')
              .paddingTop(kSpacingSmall),
          Container(
            width: kWidthFull(context),
            child: GrigoraText(
              'Take a photo',
              textAlign: TextAlign.right,
              textColor: kColorPrimary,
            ),
          ).paddingTop(kSpacingSmall)
        ],
      ).paddingAll(kSpacingMedium),
    );
  }
}
