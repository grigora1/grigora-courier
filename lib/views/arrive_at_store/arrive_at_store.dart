import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/buttons.dart';
import 'package:courier_grigora/ui_widgets/layouts/layout_with_back.dart';
import 'package:courier_grigora/ui_widgets/misc_widgets.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:courier_grigora/ui_widgets/ui_utils.dart';
import 'package:courier_grigora/views/pickup_in_progress/pickup_in_progress_screen.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';

class ArriveAtStoreScreen extends StatelessWidget {
  const ArriveAtStoreScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutWithBack(
      title: '2:51 pm | 2.9km',
      bgColor: kColorBg,
      elevation: 5,
      actions: miscWidgetAppbarActions,
      body: Column(
        children: [
          PickupWidget(),
          PickUpTitle(
            title: 'Order For',
            name: 'Customer A.',
            address: '123 Address street, \nlocated somewhere, Ikeja',
            showNavigate: false,
            showComment: true,
          ),
          PickupItem(
            title: '4 Items to Pickup',
          ),
          _WaitingForOrder(),
          GrigoraButton(
            onTap: () => UiUtils.goto(kRouteNoContactDelivery),
            text: 'Confirm Pickup',
            withBg: true,
          ).paddingAll(kSpacingMedium)
        ],
      ),
    );
  }
}

class _WaitingForOrder extends StatelessWidget {
  const _WaitingForOrder({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: kColorWhite,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Icon(
            EvaIcons.clockOutline,
            size: 24,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GrigoraTextTitle('Waiting for the order?'),
              GrigoraText('Tell us whats happening to help speed up pickups')
                  .paddingTop(kSpacingSmall),
            ],
          ).paddingLeft(kSpacingSmall).expand()
        ],
      ).paddingAll(kSpacingMedium),
    ).onTap(() => UiUtils.showBottomSheetModal(
        context: context, widget: _WaitingBottomSheet()));
  }
}

class _WaitingBottomSheet extends StatelessWidget {
  const _WaitingBottomSheet({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GrigoraTextTitle('What\'s causing your wait?'),
        GrigoraText(
          'CHECK ALL THAT APPLY',
          textColor: kColorPrimary,
        ).paddingTop(kSpacingSmall),
        Column(
          children: [
            MiscWidgetCheckboxItem(text: 'Store busy/long line'),
            MiscWidgetCheckboxItem(
                text: 'Could not get help from store employees'),
            MiscWidgetCheckboxItem(
                text: 'Order still being prepared when I arrived'),
            MiscWidgetCheckboxItem(text: 'Order not started until arrrived'),
            MiscWidgetCheckboxItem(text: 'Something else')
          ],
        ).paddingSymmetric(vertical: kSpacingMedium),
        GrigoraButton(
          onTap: () => print('wow'),
          text: 'Submit',
          withBg: true,
        ),
        GrigoraButton(
          onTap: () => UiUtils.goBack(),
          text: 'Cancel',
        ).paddingTop(kSpacingSmall)
      ],
    ).paddingAll(kSpacingMedium);
  }
}
