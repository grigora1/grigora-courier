import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/dashboard_widgets.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:courier_grigora/ui_widgets/ui_utils.dart';
import 'package:courier_grigora/utils/form_item.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';

class ScheduleScreen extends StatelessWidget {
  const ScheduleScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
          iconTheme: IconThemeData(color: kColorBlack),
          title: GrigoraTextTitle(
            'Schedule',
            textSize: kFontSizeBig,
          ),
          actions: [
            Icon(EvaIcons.options2Outline)
                .paddingAll(kSpacingSmall)
                .onTap(() => UiUtils.goto(kRouteScheduleSettings))
          ],
          elevation: 0,
          backgroundColor: kColorWhite,
        ),
        body: Container(
          color: kColorWhite,
          width: kWidthFull(context),
          height: kHeightFull(context),
          child: SingleChildScrollView(
            physics: NeverScrollableScrollPhysics(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                GrigoraTextTitle('January, 2020'),
                _Calendar().paddingTop(kSpacingMedium),
                _TabsWidget().paddingTop(kSpacingMedium)
              ],
            ).paddingAll(kSpacingMedium),
          ),
        ));
  }
}

class _Calendar extends StatelessWidget {
  const _Calendar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _items = [
      {'day': 'Today', 'number': '2', 'isActive': false},
      {'day': 'Tue', 'number': '3', 'isActive': true},
      {'day': 'Wed', 'number': '4', 'isActive': true},
      {'day': 'Thur', 'number': '5', 'isActive': false},
      {'day': 'Fri', 'number': '6', 'isActive': false},
      {'day': 'Sat', 'number': '7', 'isActive': false},
    ];
    return Row(
        children: UiUtils.grigoraDynamicWidget(
            items: _items,
            builder: (i) => _CalendarItem(
                    day: _items[i]['day'] as String,
                    number: _items[i]['number'] as String,
                    isActive: _items[i]['isActive'] as bool)
                .expand()));
  }
}

class _CalendarItem extends StatelessWidget {
  final String day, number;
  final bool isActive;
  const _CalendarItem(
      {Key? key,
      required this.day,
      required this.number,
      this.isActive = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GrigoraText(
          day.toUpperCase(),
          textSize: kFontSizeSmall,
        ),
        Container(
                decoration: BoxDecoration(
                    borderRadius: kBorderRadiusSmall,
                    color: isActive ? kColorPrimary : Colors.transparent),
                child: GrigoraText(
                  number,
                  isBoldFont: true,
                  textColor: isActive ? kColorWhite : kColorPrimaryText,
                ).paddingAll(kSpacingSmall))
            .paddingTop(kSpacingSmall),
        Container(
                width: 8,
                height: 8,
                decoration: BoxDecoration(
                    borderRadius: kFullBorderRadius,
                    color: isActive
                        ? kColorSecondary
                        : kColorBlack.withOpacity(
                            0.1,
                          )))
            .paddingTop(kSpacingSmall)
      ],
    );
  }
}

class _TabModel {
  final String title;
  final Widget body;
  _TabModel({required this.title, required this.body});
}

class _TabsWidget extends StatelessWidget {
  const _TabsWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _tabs = [
      _TabModel(title: 'Available', body: _AvailableWidget()),
      _TabModel(title: 'Scheduled', body: _ScheduledWidget())
    ];
    return DefaultTabController(
        length: _tabs.length,
        initialIndex: 0,
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                child: TabBar(
                    labelColor: kColorPrimary,
                    unselectedLabelColor: kColorPrimaryText,
                    indicatorColor: kColorPrimary,
                    labelStyle: kHeadline1TextStyle,
                    tabs: UiUtils.grigoraDynamicWidget(
                        items: _tabs,
                        builder: (i) => Tab(text: _tabs[i].title))),
              ),
              Container(
                  height: kHeightFull(context), //height of TabBarView
                  child: TabBarView(
                      children: UiUtils.grigoraDynamicWidget(
                          items: _tabs,
                          builder: (i) => SingleChildScrollView(
                                physics: BouncingScrollPhysics(),
                                child: _tabs[i]
                                    .body
                                    .paddingTop(kSpacingMedium)
                                    .paddingBottom(kSpacingExtraLarge),
                              ))))
            ]));
  }
}

class _AvailableWidget extends StatelessWidget {
  const _AvailableWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _CurrentLocation(
          title: 'Current Location',
          location: 'Maryland, Lagos',
        ),
        HorizontalDivider().paddingTop(kSpacingMedium),
        _Distance().paddingTop(kSpacingMedium),
        HorizontalDivider().paddingTop(kSpacingMedium),
        _CurrentLocation(
          title: '25km away',
          location: 'Ikeja, Lagos',
        ).paddingTop(kSpacingMedium),
      ],
    );
  }
}

class _CurrentLocation extends StatelessWidget {
  final String title, location;
  const _CurrentLocation(
      {Key? key, required this.title, required this.location})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _SectionTitle(
          title: title,
          text: location,
          buttonText: 'Schedule',
          onButtonClick: () => print('do schedule'),
        ),
        _Misc(
          body: Icon(EvaIcons.chevronUp),
        ).paddingTop(kSpacingMedium),
        _ScheduleTime().paddingTop(kSpacingMedium),
        Image.asset(kImagesDailyEarning).paddingTop(kSpacingMedium),
      ],
    );
  }
}

class _Distance extends StatelessWidget {
  const _Distance({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _SectionTitle(
          title: '25km away',
          text: 'Ketu, Lagos',
          buttonText: 'Notify me',
          onButtonClick: () => print('do schedule'),
        ),
        _Misc(
          body: GrigoraText(
            'No time sot available now',
            isCenter: true,
          ).paddingAll(kSpacingSmall),
        ).paddingTop(kSpacingMedium),
      ],
    );
  }
}

class _Misc extends StatelessWidget {
  final Widget body;
  const _Misc({Key? key, required this.body}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: kWidthFull(context),
      color: kColorBlack.withOpacity(0.05),
      child: body,
    );
  }
}

class _ScheduleTime extends StatelessWidget {
  const _ScheduleTime({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        FormItem(
          placeholder: 'Today, 12:00pm',
          label: 'Start Time',
        ).paddingRight(kSpacingSmall).expand(),
        FormItem(
          placeholder: 'Today, 12:00pm',
          label: 'End Time',
        ).paddingLeft(kSpacingSmall).expand()
      ],
    );
  }
}

class _SectionTitle extends StatelessWidget {
  final String title, text, buttonText;
  final Function onButtonClick;
  const _SectionTitle(
      {Key? key,
      required this.text,
      required this.title,
      required this.buttonText,
      required this.onButtonClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GrigoraText(
              title,
              textColor: kColorPrimary,
              isBoldFont: true,
              textSize: kFontSizeSmall,
            ),
            GrigoraTextTitle(text).paddingTop(kSpacingSmall)
          ],
        ).expand(),
        Container(
          decoration: BoxDecoration(
              color: kColorPrimary, borderRadius: kBorderRadiusSmall),
          child: GrigoraText(
            buttonText,
            textColor: kColorWhite,
          ).paddingAll(kSpacingSmall),
        ).onTap(onButtonClick)
      ],
    );
  }
}

class _ScheduledWidget extends StatelessWidget {
  const _ScheduledWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _items = [
      {
        'title': 'Lagos, Ikeja',
        'subtitle': 'January 5, 2021 at 6:30AM - 5:20 PM'
      },
      {
        'title': 'Lagos, Ikeja',
        'subtitle': 'January 5, 2021 at 6:30AM - 5:20 PM'
      }
    ];
    return Column(
        children: UiUtils.grigoraDynamicWidget(
            items: _items,
            builder: (i) => _ScheduledItem(
                  title: _items[i]['title'] ?? 'no_title',
                  subtitle: _items[i]['subtitle'] ?? 'no_subtitle',
                ).onTap(() => UiUtils.goto(kRouteScheduleChange))));
  }
}

class _ScheduledItem extends StatelessWidget {
  final String title, subtitle;
  const _ScheduledItem({Key? key, required this.title, required this.subtitle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GrigoraTextTitle(
              title,
            ),
            GrigoraText(subtitle).paddingTop(kSpacingSmall)
          ],
        ).expand(),
        Icon(EvaIcons.editOutline)
      ],
    ).paddingBottom(kSpacingMedium);
  }
}
