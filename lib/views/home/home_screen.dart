import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/buttons.dart';
import 'package:courier_grigora/ui_widgets/misc_widgets.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:courier_grigora/ui_widgets/ui_utils.dart';
import 'package:courier_grigora/utils/app_enums.dart';
import 'package:courier_grigora/utils/base_widget.dart';
import 'package:courier_grigora/views/home/home_vm.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:provider/provider.dart';
import 'package:latlong2/latlong.dart';

PreferredSizeWidget homeAppBar() => AppBar(
      leading: Icon(EvaIcons.chevronLeft).onTap(() => UiUtils.goBack()),
      iconTheme: IconThemeData(color: kColorBlack),
      title: GrigoraTextTitle('Looking for an order'),
      actions: miscWidgetAppbarActions,
      backgroundColor: kColorWhite,
    );

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final double _markerSize = 100;
  @override
  Widget build(BuildContext context) {
    var markers = <Marker>[
      Marker(
        width: _markerSize,
        height: _markerSize,
        point: LatLng(51.5, -0.09),
        builder: (ctx) => Container(
          child: _MapMarker(),
        ),
      ),
      Marker(
        width: 100.0,
        height: 100.0,
        point: LatLng(53.3498, -6.2603),
        builder: (ctx) => Container(
          child: _MapMarker(),
        ),
      ),
      Marker(
        width: _markerSize,
        height: _markerSize,
        point: LatLng(48.8566, 2.3522),
        builder: (ctx) => Container(
          child: _MapMarker(),
        ),
      ),
    ];

    return BaseWidget<HomeVM>(
        model: HomeVM(),
        builder: (context, model, child) {
          return Stack(
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: FlutterMap(
                    options: MapOptions(
                      center: LatLng(51.5, -0.09),
                      zoom: 5.0,
                    ),
                    layers: [
                      TileLayerOptions(
                        urlTemplate:
                            'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                        subdomains: ['a', 'b', 'c'],
                        // For example purposes. It is recommended to use
                        // TileProvider with a caching and retry strategy, like
                        // NetworkTileProvider or CachedNetworkTileProvider
                        tileProvider: NonCachingNetworkTileProvider(),
                      ),
                      MarkerLayerOptions(markers: markers)
                    ]),
              ),
              Positioned(child: _MapOverlay())
            ],
          );
        });
  }
}

class _MapOverlay extends StatelessWidget {
  const _MapOverlay({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Stack(
        children: [
          Positioned(top: 0, left: 0, right: 0, child: _HeaderWidget()),
          Positioned(bottom: 0, left: 0, right: 0, child: _FooterWidget())
        ],
      ).paddingAll(kSpacingMedium),
    );
  }
}

class _MapMarker extends StatelessWidget {
  const _MapMarker({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
              color: kColorPrimary.withOpacity(0.2),
              borderRadius: kFullBorderRadius),
        ),
        Container(
          width: 60,
          height: 60,
          decoration: BoxDecoration(
              color: kColorPrimary.withOpacity(0.3),
              borderRadius: kFullBorderRadius),
        ).center(),
        Container(
          width: 15,
          height: 15,
          decoration: BoxDecoration(
              color: kColorPrimary.withOpacity(0.8),
              borderRadius: kFullBorderRadius),
        ).center(),
        Stack(
          clipBehavior: Clip.none,
          children: [
            Container(
              decoration: BoxDecoration(
                  color: kColorPrimary, borderRadius: kBorderRadius),
              child: GrigoraText(
                'Very Busy',
                textColor: kColorWhite,
                isBoldFont: true,
                textSize: kFontSizeSmall,
              ).paddingSymmetric(horizontal: kSpacingSmall, vertical: 5),
            ),
            Positioned(
              left: 0,
              right: 0,
              bottom: -15,
              child: Icon(
                EvaIcons.arrowDown,
                color: kColorPrimary,
              ),
            )
          ],
        ).paddingLeft(kSpacingMedium)
      ],
    );
  }
}

class _OrderSearchHeaderWidget extends StatelessWidget {
  const _OrderSearchHeaderWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerRight,
      child: Container(
        decoration: BoxDecoration(
            color: kColorWhite,
            borderRadius: kBorderRadius,
            boxShadow: [kBoxShadow(kColorBlack.withOpacity(0.1))]),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GrigoraText(
              'Cash in hand',
              textColor: kColorPrimary,
            ),
            GrigoraTextTitle(
              'N800.00',
              textSize: kFontSizeBig,
            ).paddingTop(5)
          ],
        ).paddingAll(kSpacingMedium),
      ),
    );
  }
}

class _NavigationHeaderWidget extends StatelessWidget {
  const _NavigationHeaderWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: kColorWhite.withOpacity(0.8),
          borderRadius: kBorderRadius,
          boxShadow: [kBoxShadow(kColorBlack.withOpacity(0.1))]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              GrigoraText(
                'POSTMAN',
                isBoldFont: true,
                textSize: kFontSizeModeratelySmall,
              ),
              GrigoraText(
                'NOW',
                textSize: kFontSizeModeratelySmall,
              )
            ],
          ),
          GrigoraText(
            'Drive Now in Ikeja / Alausa / Opebi',
            textColor: kColorPrimary,
            isBoldFont: true,
            textSize: kFontSizeModeratelySmall,
          ).paddingTop(kSpacingSmall),
          GrigoraText(
            'You are now in the busy area. Tap Drive Now to start receiving orders.',
            textSize: kFontSizeModeratelySmall,
          ).paddingTop(kSpacingSmall),
          Container(
            width: kWidthFull(context),
            child: GrigoraText(
              'Tap to go Online',
              textDecoration: TextDecoration.underline,
              textAlign: TextAlign.end,
              textColor: kColorPrimary,
              isBoldFont: true,
              textSize: kFontSizeModeratelySmall,
            ).paddingTop(kSpacingSmall),
          )
        ],
      ).paddingAll(kFontSizeModeratelySmall),
    );
  }
}

class _DefaultHeaderWidget extends StatelessWidget {
  const _DefaultHeaderWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var model = Provider.of<HomeVM>(context);
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Column(children: [
          _MapIcon(
            icon: EvaIcons.bellOutline,
          ).onTap(() => UiUtils.goto(kRouteNotifications)),
          _MapIcon(
            icon: EvaIcons.emailOutline,
          )
              .onTap(() => UiUtils.goto(kRouteMessages))
              .paddingTop(kSpacingMedium),
          _MapIcon(
            icon: EvaIcons.awardOutline,
          )
              .onTap(() => UiUtils.goto(kRouteChallenges))
              .paddingTop(kSpacingMedium),
        ]),
        _PauseSprints().visible(model.state == EnumHomeState.busyLocation),
        _MapIcon(
          icon: EvaIcons.questionMarkCircle,
          color: kColorPrimary.withOpacity(0.7),
          paddingDouble: 1,
          size: 32,
        ).onTap(() => UiUtils.goto(kRoutePostmanHelp)),
      ],
    );
  }
}

class _HeaderWidget extends StatelessWidget {
  const _HeaderWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var model = Provider.of<HomeVM>(context);
    return Stack(
      children: [
        _DefaultHeaderWidget().visible(model.showDefaultHeaderWidget()),
        _NavigationHeaderWidget()
            .visible(model.state == EnumHomeState.navigate),
        _OrderSearchHeaderWidget()
            .visible(model.state == EnumHomeState.orderSearch)
      ],
    );
  }
}

class _PauseSprints extends StatelessWidget {
  const _PauseSprints({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: kColorWhite,
          borderRadius: kFullBorderRadius,
          boxShadow: [kBoxShadow(kColorBlack.withOpacity(0.1))]),
      child: GrigoraText(
        'Pause Sprints',
        isBoldFont: true,
      )
          .paddingSymmetric(vertical: 5, horizontal: 10)
          .onTap(() => UiUtils.goto(kRoutePausedSprint)),
    );
  }
}

class _MapIcon extends StatelessWidget {
  final IconData icon;

  final Color? color;
  final double? paddingDouble, size;
  const _MapIcon(
      {Key? key, required this.icon, this.color, this.paddingDouble, this.size})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: kColorWhite,
          borderRadius: kFullBorderRadius,
          boxShadow: [kBoxShadow(kColorBlack.withOpacity(0.1))]),
      child: Icon(
        icon,
        color: color ?? kColorPrimaryText,
        size: size ?? 20,
      ).paddingAll(paddingDouble ?? 8),
    );
  }
}

class _PostmanFooterWidget extends StatelessWidget {
  const _PostmanFooterWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _FooterNavigation(),
        _FooterInfo(
          title: 'Lagos: Allen/Ikeja',
          description:
              'It\'s not busy in this area. Navigate to a busier area where there will be more orders.',
        ).paddingTop(kSpacingMedium)
      ],
    );
  }
}

class _SprintFooterWidget extends StatelessWidget {
  const _SprintFooterWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var model = Provider.of<HomeVM>(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _FooterInfo(
          title: 'Lagos: Ikeja, Alausa, Ogba',
          buttonWidget: GrigoraButton(
            onTap: () => UiUtils.showBottomSheetModal(
              context: context,
              widget: _DeliveryPack(model),
            ),
            text: 'Go Online',
            withBg: true,
          ).paddingTop(kSpacingMedium),
        )
      ],
    );
  }
}

class _DeliveryPack extends StatelessWidget {
  final HomeVM model;
  const _DeliveryPack(this.model, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: kWidthFull(context),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            alignment: Alignment.centerRight,
            child: Icon(
              EvaIcons.closeCircle,
              color: kColorBlack.withOpacity(0.3),
            ),
          ),
          GrigoraTextTitle(
            'Delivery Pack',
            textSize: kFontSizeBig,
          ),
          GrigoraText(
                  'Please ensure you have these items before receiving a delivery offer')
              .paddingTop(kSpacingMedium),
          MiscWidgetCheckboxItem(
            text: "Charged Phone",
          ).paddingTop(kSpacingMedium),
          MiscWidgetCheckboxItem(
            text: "Enough fuel",
          ),
          MiscWidgetCheckboxItem(
            text: "Hot bag",
          ),
          GrigoraButton(
            onTap: () {
              model.state = EnumHomeState.orderSearch;
              UiUtils.goBack();
            },
            text: 'Start',
            withBg: true,
          ).paddingTop(kSpacingMedium)
        ],
      ).paddingAll(kSpacingMedium),
    );
  }
}

class _NavigateFooterWidget extends StatelessWidget {
  const _NavigateFooterWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var model = Provider.of<HomeVM>(context);
    return Container(
      width: kWidthFull(context),
      decoration: BoxDecoration(
          color: kColorWhite,
          borderRadius: kBorderRadius,
          boxShadow: [kBoxShadow(kColorBlack.withOpacity(0.1))]),
      child: Row(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GrigoraText(
                '14 mins',
                isBoldFont: true,
                textColor: kColorAccent,
              ),
              GrigoraText('1.5KM, 10:30 AM').paddingTop(kSpacingSmall)
            ],
          ).expand(),
          Container(
            decoration: BoxDecoration(
                color: kColorPrimary, borderRadius: kBorderRadius),
            child: GrigoraText(
              'Exit',
              textColor: kColorWhite,
              isBoldFont: true,
            )
                .paddingSymmetric(vertical: 8, horizontal: kSpacingBigMedium)
                .onTap(() => model.state = EnumHomeState.sprint),
          )
        ],
      ).paddingAll(kSpacingMedium),
    );
  }
}

class _BusyLocationFooterWidget extends StatelessWidget {
  const _BusyLocationFooterWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var model = Provider.of<HomeVM>(context);
    return _FooterInfo(
      title: 'Lagos: Maryland',
      description:
          'It\'s not busy in this area. Navigate to a businer area where there will be more orders',
      buttonWidget: GrigoraButton(
        onTap: () => model.state = EnumHomeState.navigate,
        icon: Icon(
          Icons.directions_outlined,
          size: 20,
          color: kColorWhite,
        ).paddingRight(5),
        text: 'Navigate',
        withBg: true,
      ).paddingTop(kSpacingMedium),
    );
  }
}

class _OrderSearchFooterWidget extends StatelessWidget {
  const _OrderSearchFooterWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var model = Provider.of<HomeVM>(context);
    return Container(
      width: kWidthFull(context),
      decoration: BoxDecoration(
          color: kColorWhite,
          borderRadius: kBorderRadius,
          boxShadow: [kBoxShadow(kColorBlack.withOpacity(0.1))]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GrigoraTextTitle('Lagos: Ikeja, Alausa, Ogba'),
          GrigoraText(
            'You are currently online',
            isBoldFont: true,
            textColor: kColorPrimary,
          ).paddingTop(kSpacingMedium),
          Row(
            children: [
              Icon(
                Icons.money,
                size: 24,
              ),
              GrigoraText('N200 peak pay until 3:30M')
                  .paddingLeft(kSpacingSmall)
                  .expand(),
              Icon(EvaIcons.chevronRight)
            ],
          ).paddingTop(kSpacingSmall),
          GrigoraButton(
            onTap: () => UiUtils.showBottomSheetModal(
                context: context, widget: _AcceptOrderModal()),
            text: 'Go Offline',
            withBg: true,
          ).paddingTop(kSpacingMedium),
        ],
      ).paddingAll(kSpacingMedium),
    );
  }
}

class _OrderRequestFooterWidget extends StatelessWidget {
  const _OrderRequestFooterWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var model = Provider.of<HomeVM>(context);
    return Container(
      width: kWidthFull(context),
      decoration: BoxDecoration(
          color: kColorWhite,
          borderRadius: kBorderRadius,
          boxShadow: [kBoxShadow(kColorBlack.withOpacity(0.1))]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GrigoraTextTitle('Lagos: Ikeja, Alausa, Ogba'),
          GrigoraText(
            'You are currently online',
            isBoldFont: true,
            textColor: kColorPrimary,
          ).paddingTop(kSpacingMedium),
          Row(
            children: [
              Icon(
                Icons.money,
                size: 24,
              ),
              GrigoraText('N200 peak pay until 3:30M')
                  .paddingLeft(kSpacingSmall)
                  .expand(),
              Icon(EvaIcons.chevronRight)
            ],
          ).paddingTop(kSpacingSmall),
          GrigoraButton(
            onTap: () => print('hello'),
            text: 'Go Offline',
            withBg: true,
          ).paddingTop(kSpacingMedium),
        ],
      ).paddingAll(kSpacingMedium),
    );
  }
}

class _AcceptOrderModal extends StatelessWidget {
  const _AcceptOrderModal({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              _Tag('Custom Order'),
              _CashTag('Cash'),
              _TimeTag('01:25')
            ],
          ),
          Container(
            width: kWidthFull(context),
            decoration: BoxDecoration(color: kColorPrimary.withOpacity(0.1)),
            child: GrigoraText('Shop for items at the store then deliver')
                .paddingAll(kSpacingSmall),
          ).paddingTop(kSpacingMedium),
          GrigoraTextTitle('Chicken CO.').paddingTop(kSpacingMedium),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              GrigoraText(
                'Item Cost: 1500',
                textColor: kColorPrimary,
                isBoldFont: true,
              ),
              GrigoraText(
                '8 items 4.9km',
              )
            ],
          ).paddingTop(kSpacingSmall),
          GrigoraText(
            'You will be receiving cash at the delivery point',
            textSize: kFontSizeNormal,
          ).paddingTop(kSpacingSmall),
          Container(
            width: kWidthFull(context),
            child: GrigoraText(
              'Show detail instruction',
              textAlign: TextAlign.right,
              textColor: kColorPrimary,
            ).paddingTop(kSpacingMedium),
          ),
          _Destination().paddingTop(kSpacingSmall),
          Container(
            width: kWidthFull(context),
            child: GrigoraTextTitle(
              'N800.00',
              textSize: kFontSizeBig,
              isCenter: true,
            ),
          ).paddingTop(kSpacingMedium),
          Container(
            width: kWidthFull(context),
            child: GrigoraText(
              '(Includes Dzooco pay and customer tip)',
              isCenter: true,
            ),
          ).paddingTop(kSpacingSmall),
          GrigoraButton(
            onTap: () => UiUtils.goto(kRoutePickupInProgress),
            text: 'Accept',
            withBg: true,
          ).paddingTop(kSpacingMedium),
          GrigoraButton(
            onTap: () => UiUtils.goto(kRouteDeclineReason),
            text: 'Decline',
          ).paddingTop(kSpacingSmall)
        ],
      ),
    ).paddingAll(kSpacingMedium);
  }
}

class _Destination extends StatelessWidget {
  const _Destination({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Icon(
              EvaIcons.pinOutline,
              size: 24,
            ),
            GrigoraText('Tanke Ilewe').paddingLeft(kSpacingSmall)
          ],
        ),
        Container(
          width: 1,
          height: 10,
          color: kColorBlack,
        ).paddingLeft(11),
        Row(
          children: [
            Icon(
              EvaIcons.radioButtonOn,
              size: 24,
              color: kColorAccent,
            ),
            GrigoraText('Post Office').paddingLeft(kSpacingSmall)
          ],
        ),
      ],
    );
  }
}

class _TimeTag extends StatelessWidget {
  final String text;
  const _TimeTag(this.text, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: kBorderRadius, color: kColorPrimary.withOpacity(0.1)),
      child: Row(
        children: [
          Icon(
            EvaIcons.clockOutline,
            color: kColorPrimary,
            size: 24,
          ),
          GrigoraText(
            text,
          ).paddingLeft(kSpacingSmall),
        ],
      ).paddingSymmetric(vertical: 5, horizontal: kSpacingSmall),
    );
  }
}

class _CashTag extends StatelessWidget {
  final String text;
  const _CashTag(this.text, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: kFullBorderRadius,
          color: kColorBlack.withOpacity(0.05)),
      child: GrigoraText(
        text,
      ).paddingSymmetric(vertical: 5, horizontal: 10),
    );
  }
}

class _Tag extends StatelessWidget {
  final String text;
  const _Tag(this.text, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: kFullBorderRadius,
          color: kColorPrimary.withOpacity(0.6)),
      child: GrigoraText(
        text,
        textColor: kColorWhite,
      ).paddingSymmetric(vertical: 5, horizontal: 10),
    );
  }
}

Widget _getFooterWidget(HomeVM model) {
  switch (model.state) {
    case EnumHomeState.postman:
      return _PostmanFooterWidget();
    case EnumHomeState.sprint:
      return _SprintFooterWidget();
    case EnumHomeState.busyLocation:
      return _BusyLocationFooterWidget();
    case EnumHomeState.navigate:
      return _NavigateFooterWidget();
    case EnumHomeState.orderSearch:
      return _OrderSearchFooterWidget();
    case EnumHomeState.orderRequest:
      return _OrderRequestFooterWidget();

    default:
      return _PostmanFooterWidget();
  }
}

class _FooterWidget extends StatelessWidget {
  const _FooterWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var model = Provider.of<HomeVM>(context);
    return _getFooterWidget(model);
  }
}

class _FooterNavigation extends StatelessWidget {
  const _FooterNavigation({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(mainAxisAlignment: MainAxisAlignment.end, children: [
      _VehicleType(),
      _LocationSelect().paddingLeft(kSpacingSmall)
    ]);
  }
}

class _LocationSelect extends StatelessWidget {
  const _LocationSelect({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration:
          BoxDecoration(color: kColorWhite, borderRadius: kFullBorderRadius),
      child: Icon(
        EvaIcons.compassOutline,
        size: 24,
      ).paddingAll(5),
    );
  }
}

class _VehicleType extends StatelessWidget {
  const _VehicleType({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration:
          BoxDecoration(color: kColorWhite, borderRadius: kFullBorderRadius),
      child: Row(
        children: [GrigoraText('Motocycle'), Icon(EvaIcons.chevronDownOutline)],
      ).paddingSymmetric(vertical: 5, horizontal: kSpacingSmall),
    );
  }
}

class _FooterInfo extends StatelessWidget {
  final String title;
  final String? description;
  final Widget? buttonWidget;
  const _FooterInfo(
      {Key? key, required this.title, this.description, this.buttonWidget})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: kWidthFull(context),
      decoration: BoxDecoration(
          color: kColorWhite.withOpacity(0.9),
          borderRadius: kBorderRadius,
          boxShadow: [kBoxShadow(kColorBlack.withOpacity(0.1))]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GrigoraTextTitle(title),
          GrigoraText(description ?? '')
              .paddingTop(kSpacingMedium)
              .visible(description != null),
          buttonWidget ?? Container()
        ],
      ).paddingAll(kSpacingMedium),
    );
  }
}
