import 'package:intl/intl.dart';
import 'package:flutter/material.dart';

const String kThemeLocal = 'THEME_DATA';
const String kAppName = 'Grigora Courier';

const double kSpacingSmall = 10;
const double kSpacingMedium = 24;
const double kSpacingBigMedium = 30;
const double kSpacingLarge = 40;
const double kSpacingExtraLarge = 150;

// Border
const double kBorderWidth = 1;
const double kThickBorderWidth = 3;
const BorderRadius kBorderRadius =
    BorderRadius.all(const Radius.circular(kSpacingSmall));
const BorderRadius kBorderRadiusSmall =
    BorderRadius.all(const Radius.circular(5));
const BorderRadius kFullBorderRadius =
    BorderRadius.all(const Radius.circular(100));
final BoxDecoration kTextFieldBoxDecoration = BoxDecoration(
    borderRadius: kBorderRadius,
    border: Border.all(color: kThemeData.dividerColor),
    color: Colors.white);
final BoxDecoration kButtonBoxDecoration = BoxDecoration(
    borderRadius: kFullBorderRadius, border: Border.all(color: kColorPrimary));
final BoxDecoration kBottomSheetBoxDecoration = BoxDecoration(
  color: Colors.white,
  borderRadius: new BorderRadius.only(
    topLeft: const Radius.circular(25.0),
    topRight: const Radius.circular(25.0),
  ),
);

// Box Shadow
BoxShadow kBoxShadow(Color color) => BoxShadow(
      color: color,
      spreadRadius: 0,
      blurRadius: 15,
      offset: Offset(0, 2), // changes position of shadow
    );

// Colors
const Color kColorPrimary = Color(0xFFCA3114);
const Color kColorAccent = Color(0xFF0DB4B9);
const Color kColorSecondary = Color(0xFFF6A935);
const Color kColorPrimaryLight = Color(0xFFF7D6D6);
const Color kColorPurple = Color(0xFFA5A6F6);
const Color kColorGold = Color(0xFFFD9942);
const Color kColorPrimaryText = Colors.black;
final Color kColorWhite = Colors.white;
final Color kColorBg = Color(0xFFFF9F9F9);
const Color kSecondaryTextColor = Colors.white;
const Color kScaffoldBackgroundColor = Colors.white;
final Color kSubtextColor = Colors.grey[400]!;
final Color kColorBlack = Colors.black;
final Color kColorGrey = Colors.grey[300]!;
final Color kColorGreyLight = Colors.grey[100]!;
final Color kColorGreen = Color(0xFF3CC13B);
final Color kColorDarkGrey = Color(0xFF707070);
final Color kColorFacebook = Color(0xFF4267B2);
final Color kColorGoogle = Color(0xFF4285F4);

// FontSize
const double kFontSizeSmall = 12;
const double kFontSizeModeratelySmall = 14;
const double kFontSizeNormal = 16;
const double kFontSizeMedium = 18;
const double kFontSizeLarge = 20;
final double kFontSizeBig = 24;
final double kFontSizeIconSmall = 20;
final double kFontSizeIconBig = 32;

// fonts
const String kFontFamilyLight = 'AvertaStd-Light';
const String kFontFamilyRegular = 'AvertaStd-Regular';
const String kFontFamilySemibold = 'AvertaStd-Semibold';

// Text
final TextStyle kHeadline1TextStyle = TextStyle(
    fontSize: kFontSizeLarge,
    color: kColorPrimaryText,
    fontFamily: kFontFamilySemibold);
final TextStyle kHeadline2TextStyle = TextStyle(
    fontSize: 25, fontWeight: FontWeight.normal, color: kColorPrimaryText);
final TextStyle kHeadline3TextStyle = TextStyle(
    fontSize: 21, fontWeight: FontWeight.bold, color: kColorPrimaryText);
final TextStyle kBodyText1Style = TextStyle(
    fontSize: kFontSizeNormal,
    color: kColorPrimaryText,
    fontFamily: kFontFamilyRegular);
final TextStyle kBodyTextBold = TextStyle(
    fontSize: kFontSizeNormal,
    color: kColorPrimaryText,
    fontFamily: kFontFamilySemibold);
final TextStyle kBodyText3Style = TextStyle(
    fontSize: 18, fontWeight: FontWeight.bold, color: kColorPrimaryText);
final TextStyle kSubtitle1Style = TextStyle(fontSize: 12, color: kSubtextColor);
final TextStyle kSubtitle2Style =
    TextStyle(fontSize: 12, color: kColorPrimaryText);

// Misc functions
String kPriceFormatter(double price) =>
    '\$' + NumberFormat("#,##0.00", "en_US").format(price);
List kBuilderName(String name) => name.split(' ');

// Theme
final ThemeData kThemeData = ThemeData.light().copyWith(
    primaryColor: kColorPrimary,
    scaffoldBackgroundColor: kScaffoldBackgroundColor,
    dividerColor: Colors.grey[350]!,
    visualDensity: VisualDensity.adaptivePlatformDensity,
    iconTheme: IconThemeData(size: kFontSizeIconBig),
    textTheme: TextTheme(
        headline1: kHeadline1TextStyle,
        headline2: kHeadline2TextStyle,
        headline3: kHeadline3TextStyle,
        bodyText1: kBodyText1Style,
        bodyText2: kBodyTextBold,
        subtitle1: kSubtitle1Style,
        subtitle2: kSubtitle2Style));

final ThemeData kThemeDataDark = ThemeData.dark().copyWith(
    primaryColor: kColorPrimary,
    primaryColorDark: Colors.black,
    primaryColorLight: Colors.white,
    scaffoldBackgroundColor: Colors.grey[900],
    dividerColor: Colors.grey[350]!,
    visualDensity: VisualDensity.adaptivePlatformDensity,
    iconTheme: IconThemeData(size: kFontSizeIconBig),
    textTheme: TextTheme(
        headline1: kHeadline1TextStyle.copyWith(color: Colors.white),
        headline2: kHeadline2TextStyle.copyWith(color: Colors.white),
        headline3: kHeadline3TextStyle.copyWith(color: Colors.white),
        bodyText1: kBodyText1Style.copyWith(color: Colors.white),
        bodyText2: kBodyTextBold.copyWith(color: Colors.white),
        subtitle1: kSubtitle1Style.copyWith(color: Colors.white),
        subtitle2: kSubtitle2Style.copyWith(color: Colors.white)));

// Routes
const kRouteOnboarding = "/onboarding";
const kRouteCountrySelect = "/country-select";
const kRouteLogin = '/login';
const kRouteLoginEmail = '/login-email';
const kRouteCreateAccount = '/create-account';
const kRouteForgotPassword = "/forgot-password";
const kRouteHome = '/home';
const kRouteRestaurants = '/restaurants';
const kRouteAbout = '/about';
const kRouteBankDetails = '/bank-details';
const kRouteVehicleDetails = '/vehicle-details';
const kRouteVehicleInformation = '/vehicle-information';
const kRouteScheduleChange = '/schedule-change';
const kRouteScheduleDelete = '/schedule-delete';
const kRouteScheduleSettings = '/schedule-settings';
const kRoutePostmanHelp = '/postman-help';
const kRouteProfile = '/profile';
const kRoutePerformance = '/performance';
const kRouteHelp = '/help';
const kRouteNotifications = '/notifications';
const kRouteMessages = '/messages';
const kRouteChallenges = '/challenges';
const kRouteChallengeNew = '/challenges-new';
const kRouteChallengeActive = '/challenges-active';
const kRouteChallengePast = '/challenges-past';
const kRouteVerifyOTP = '/verify-otp';
const kRouteCreateAccount2 = '/create-account-2';
const kRouteOnboardingSummary = '/onboarding-summary';
const kRoutePausedSprint = '/paused-sprint';
const kRouteDeclineReason = '/decline-reason';
const kRouteDeclineAccept = '/decline-accept';
const kRoutePickupInProgress = '/pickup-in-progress';
const kRouteArriveAtStore = '/arrive-at-store';
const kRouteNoContactDelivery = '/no-contact-delivery';
const kRouteCurrentSchedule = '/current-schedule';
const kRouteAcceptance = '/acceptance';
const kRouteFinalSteps = '/final-steps';
const kRouteEarnings = '/earnings';
const kRouteEarningsDaily = '/earnings-daily';
const kRouteBackgroundCheck = '/background-check';
const kRouteDashboard = '/dashboard';
const kRouteReferral = '/referral';

// Images
const String kImagesGrigoraLogo = 'images/logo.png';
const String kImagesGoogleIcon = 'images/icon-google.png';
const String kImagesFacebookIcon = 'images/icon-facebook.svg';
const String kImagesAppleIcon = 'images/icon-apple.svg';
const String kImagesPay = 'images/pay.svg';
const String kImagesLogin = 'images/login.svg';
const String kImagesEmpty = 'images/empty_icon.png';
const String kImagesPlaceholder = 'images/placeholder.png';
const String kImagesDailyEarning = 'images/daily_earning.png';
const String kImagesRemotePlaceholder =
    'https://media.istockphoto.com/vectors/thumbnail-image-vector-graphic-vector-id1147544807?k=6&m=1147544807&s=612x612&w=0&h=8CXEtGfDlt7oFx7UyEZClHojvDjZR91U-mAU8UlFF4Y=';
const String kImagesRice = 'images/rice-section-1.jpg';
const String kImagesBreakfast = 'images/breakfast-section-1.jpg';
const String kImagesCard = 'images/card.png';
const String kImagesHowToDashScreenshot =
    'images/how-to-dash-screenshot-1-dash-now.png';

// svg_icons
const String kImagesIconLocation = 'images/icon-location.svg';
const String kImagesIconCart = 'images/icon-cart.svg';
const String kImagesIconChat = 'images/icon-chat.svg';
const String kImagesIconVerified = 'images/icon-verified.svg';
const String kImagesIconCashPayment = 'images/icon-cash-payment.svg';
const String kImagesIconCardPayment = 'images/icon-card-payment.svg';
const String kImagesIconClock = 'images/icon-clock.svg';
const String kImagesIconSchedule = 'images/icon-schedule.svg';
const String kImagesIconNaira = 'images/icon-naira.svg';
const String kImagesIconStart = 'images/icon-start.svg';
const String kImagesIconEnd = 'images/icon-end.svg';

// width
double kWidthFull(BuildContext context) {
  return MediaQuery.of(context).size.width;
}

double kHeightFull(BuildContext context) {
  return MediaQuery.of(context).size.height;
}

// misc functions
kGotoScreen(BuildContext context, String routeName, {Object? arguments}) {
  Navigator.pushNamed(context, routeName, arguments: arguments);
}

// padding
const EdgeInsetsGeometry kPaddingAllSmall = const EdgeInsets.all(kSpacingSmall);
const EdgeInsetsGeometry kPaddingAllMedium =
    const EdgeInsets.all(kSpacingMedium);
const EdgeInsetsGeometry kPaddingAllLarge = const EdgeInsets.all(kSpacingLarge);

// misc constants
const TextAlign kTextAlignCenter = TextAlign.center;

// Endpoint
const _base_url = 'http://143.110.237.114:4001/api/v1';
const kEndpointRegister = _base_url + '/users/auth/register';
const kEndpointLogin = _base_url + '/users/auth/login';
const kEndpointVerifyOTP = _base_url + '/users/auth/validate_otp';
const kEndpointResendOTP = _base_url + '/users/auth/resend';
