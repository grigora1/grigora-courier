import 'package:courier_grigora/views/acceptance/acceptance_screen.dart';
import 'package:courier_grigora/views/arrive_at_store/arrive_at_store.dart';
import 'package:courier_grigora/views/background_check/background_check_screen.dart';
import 'package:courier_grigora/views/bank_details/bank_details_screen.dart';
import 'package:courier_grigora/views/challenge_active/challenge_active_screen.dart';
import 'package:courier_grigora/views/challenge_new/challenge_new_screen.dart';
import 'package:courier_grigora/views/challenges/challenges_screen.dart';
import 'package:courier_grigora/views/challenges_past/challenges_past_screen.dart';
import 'package:courier_grigora/views/current_schedule/current_schedule_screen.dart';
import 'package:courier_grigora/views/dashboard/dashboard_screen.dart';
import 'package:courier_grigora/views/decline_accept/decline_accept_screen.dart';
import 'package:courier_grigora/views/decline_reason/decline_reason.dart';
import 'package:courier_grigora/views/earnings/earnings_screen.dart';
import 'package:courier_grigora/views/earnings_daily/earnings_daily_screen.dart';
import 'package:courier_grigora/views/final_steps/final_steps_screen.dart';
import 'package:courier_grigora/views/messages/messages_screen..dart';
import 'package:courier_grigora/views/no_contact_delivery/no_contact_delivery_screen.dart';
import 'package:courier_grigora/views/onboarding_summary/onboarding_summary_screen.dart';
import 'package:courier_grigora/views/paused_sprints/paused_sprints_screen.dart';
import 'package:courier_grigora/views/performance/performance_screen.dart';
import 'package:courier_grigora/views/pickup_in_progress/pickup_in_progress_screen.dart';
import 'package:courier_grigora/views/postman_help/postman_help_screen.dart';
import 'package:courier_grigora/views/profile/profile_screen.dart';
import 'package:courier_grigora/views/referral/referral_screen.dart';
import 'package:courier_grigora/views/schedule_change/schedule_change_screen.dart';
import 'package:courier_grigora/views/schedule_delete/schedule_delete_screen.dart';
import 'package:courier_grigora/views/schedule_settings/schedule_settings_screen.dart';
import 'package:courier_grigora/views/vehicle_details/vehicle_details_screen.dart';
import 'package:courier_grigora/views/vehicle_information/vehicle_information_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/views/about_screen.dart';
import 'package:courier_grigora/views/country_select/country_select_screen.dart';
import 'package:courier_grigora/views/create_account/create_account_screen.dart';
import 'package:courier_grigora/views/create_account_2/create_account_2_screen.dart';
import 'package:courier_grigora/views/help_screen.dart';
import 'package:courier_grigora/views/login_email/login_email_screen.dart';
import 'package:courier_grigora/views/notifications/notifications.dart';
import 'package:courier_grigora/views/login/login_screen.dart';
import 'package:courier_grigora/views/forgot_password/forgot_password_screen.dart';
import 'package:courier_grigora/views/onboarding_screen.dart';

class AppPages {
  AppPages._();
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case kRouteLogin:
        return new MyCustomRoute(
          builder: (_) => new LoginScreen(),
          settings: settings,
        );
      case kRouteLoginEmail:
        return new MyCustomRoute(
          builder: (_) => new LoginEmailScreen(),
          settings: settings,
        );
      case kRouteCreateAccount:
        return new MyCustomRoute(
          builder: (_) => new CreateAccountScreen(),
          settings: settings,
        );
      case kRouteCreateAccount2:
        return new MyCustomRoute(
          builder: (_) => new CreateAccount2Screen(),
          settings: settings,
        );
      case kRouteVehicleDetails:
        return new MyCustomRoute(
          builder: (_) => new VehicleDetailsScreen(),
          settings: settings,
        );
      case kRouteBackgroundCheck:
        return new MyCustomRoute(
          builder: (_) => new BackgroundCheckScreen(),
          settings: settings,
        );
      case kRouteOnboardingSummary:
        return new MyCustomRoute(
          builder: (_) => new OnboardingSummaryScreen(),
          settings: settings,
        );
      case kRouteFinalSteps:
        return new MyCustomRoute(
          builder: (_) => new FinalStepsScreen(),
          settings: settings,
        );
      case kRouteDashboard:
        return new MyCustomRoute(
          builder: (_) => new DashboardScreen(),
          settings: settings,
        );
      case kRouteCountrySelect:
        return new MyCustomRoute(
          builder: (_) => new CountrySelectScreen(),
          settings: settings,
        );
      case kRouteForgotPassword:
        return new MyCustomRoute(
          builder: (_) => new ForgotPasswordScreen(),
          settings: settings,
        );
      case kRouteAbout:
        return new MyCustomRoute(
          builder: (_) => new AboutScreen(),
          settings: settings,
        );
      case kRouteBankDetails:
        return new MyCustomRoute(
          builder: (_) => new BankDetailsScreen(),
          settings: settings,
        );
      case kRouteHelp:
        return new MyCustomRoute(
          builder: (_) => new HelpScreen(),
          settings: settings,
        );
      case kRouteNotifications:
        return new MyCustomRoute(
          builder: (_) => new NotificationsScreen(),
          settings: settings,
        );
      case kRouteMessages:
        return new MyCustomRoute(
          builder: (_) => new MessagesScreen(),
          settings: settings,
        );
      case kRouteChallenges:
        return new MyCustomRoute(
          builder: (_) => new ChallengesScreen(),
          settings: settings,
        );
      case kRouteChallengeNew:
        return new MyCustomRoute(
          builder: (_) => new ChallengeNewScreen(),
          settings: settings,
        );
      case kRouteChallengeActive:
        return new MyCustomRoute(
          builder: (_) => new ChallengeActiveScreen(),
          settings: settings,
        );
      case kRouteChallengePast:
        return new MyCustomRoute(
          builder: (_) => new ChallengePastScreen(),
          settings: settings,
        );
      case kRoutePausedSprint:
        return new MyCustomRoute(
          builder: (_) => new PausedSprintScreen(),
          settings: settings,
        );
      case kRouteDeclineReason:
        return new MyCustomRoute(
          builder: (_) => new DeclineReasonScreen(),
          settings: settings,
        );
      case kRouteDeclineAccept:
        return new MyCustomRoute(
          builder: (_) => new DeclineAcceptScreen(),
          settings: settings,
        );
      case kRoutePickupInProgress:
        return new MyCustomRoute(
          builder: (_) => new PickupInProgressScreen(),
          settings: settings,
        );
      case kRouteArriveAtStore:
        return new MyCustomRoute(
          builder: (_) => new ArriveAtStoreScreen(),
          settings: settings,
        );
      case kRouteCurrentSchedule:
        return new MyCustomRoute(
          builder: (_) => new CurrentScheduleScreen(),
          settings: settings,
        );
      case kRouteNoContactDelivery:
        return new MyCustomRoute(
          builder: (_) => new NoContactDeliveryScreen(),
          settings: settings,
        );
      case kRouteReferral:
        return new MyCustomRoute(
          builder: (_) => new ReferralScreen(),
          settings: settings,
        );
      case kRouteVehicleInformation:
        return new MyCustomRoute(
          builder: (_) => new VehicleInformationScreen(),
          settings: settings,
        );
      case kRouteProfile:
        return new MyCustomRoute(
          builder: (_) => new ProfileScreen(),
          settings: settings,
        );
      case kRoutePerformance:
        return new MyCustomRoute(
          builder: (_) => new PerformanceScreen(),
          settings: settings,
        );
      case kRouteAcceptance:
        return new MyCustomRoute(
          builder: (_) => new AcceptanceScreen(),
          settings: settings,
        );
      case kRouteEarnings:
        return new MyCustomRoute(
          builder: (_) => new EarningsScreen(),
          settings: settings,
        );
      case kRouteEarningsDaily:
        return new MyCustomRoute(
          builder: (_) => new EarningsDailyScreen(),
          settings: settings,
        );
      case kRouteScheduleChange:
        return new MyCustomRoute(
          builder: (_) => new ScheduleChangeScreen(),
          settings: settings,
        );
      case kRouteScheduleDelete:
        return new MyCustomRoute(
          builder: (_) => new ScheduleDeleteScreen(),
          settings: settings,
        );
      case kRouteScheduleSettings:
        return new MyCustomRoute(
          builder: (_) => new ScheduleSettingsScreen(),
          settings: settings,
        );
      case kRoutePostmanHelp:
        return new MyCustomRoute(
          builder: (_) => new PostmanHelpScreen(),
          settings: settings,
        );
      // case kRouteVerifyOTP:
      //   return new MyCustomRoute(
      //     builder: (_) => new VerifyOTP(),
      //     settings: settings,
      //   );
      default:
        return new MyCustomRoute(
          builder: (_) => new OnboardingScreen(),
          settings: settings,
        );
    }
    // switch (settings.name) {
    //   case '/':
    //     return GetRoute(
    //       builder: (_) => SplashScreen(),
    //       settings: settings,
    //     );
    //   case '/Home':
    //     return GetRoute(settings: settings, builder: (_) => Home(), transition: Transition.fade);
    //   case '/Chat':
    //     return GetRoute(settings: settings, builder: (_) => Chat(),transition: Transition.rightToLeft);
    //   default:
    //     return GetRoute(
    //         settings: settings,
    //         transition: Transition.rotate
    //         builder: (_) => Scaffold(
    //               body: Center(
    //                   child: Text('No route defined for ${settings.name}')),
    //             ));
    // }
  }
}

class MyCustomRoute<T> extends MaterialPageRoute<T> {
  MyCustomRoute({required WidgetBuilder builder, RouteSettings? settings})
      : super(builder: builder, settings: settings);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    // if (settings.isInitialRoute) return child;
    return new FadeTransition(opacity: animation, child: child);
  }
}
