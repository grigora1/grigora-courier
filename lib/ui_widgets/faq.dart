import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:courier_grigora/utils/base_model.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:courier_grigora/utils/base_widget.dart';
import 'package:courier_grigora/constants.dart';
import 'package:nb_utils/nb_utils.dart';

class _FaqItemVM extends BaseModel {
  bool _isOpen = false;
  bool get isOpen => _isOpen;
  set isOpen(bool value) {
    _isOpen = value;
    notifyListeners();
  }
}

class FaqItemWidget extends StatelessWidget {
  final String title, subtitle;
  const FaqItemWidget({Key? key, required this.title, required this.subtitle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseWidget<_FaqItemVM>(
        model: _FaqItemVM(),
        builder: (context, model, child) {
          return Container(
            padding: kPaddingAllMedium,
            decoration: BoxDecoration(
                color: kColorWhite, boxShadow: [kBoxShadow(kColorGreyLight)]),
            child: Column(
              children: [
                Row(
                  children: [
                    GrigoraTextTitle(
                      title,
                      textColor:
                          model.isOpen ? kColorPrimary : kColorPrimaryText,
                    ).expand(),
                    Icon(
                      model.isOpen ? EvaIcons.chevronUp : EvaIcons.chevronDown,
                      color: model.isOpen ? kColorPrimary : kColorPrimaryText,
                    )
                  ],
                ),
                GrigoraText(subtitle)
                    .paddingTop(kSpacingSmall)
                    .visible(model.isOpen)
              ],
            ),
          ).onTap(() => model.isOpen = !model.isOpen);
        });
  }
}
