import 'package:flutter/material.dart';
import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:courier_grigora/ui_widgets/ui_utils.dart';

class GrigoraTabWidget extends StatelessWidget {
  const GrigoraTabWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<_TabModel> _items = [
      _TabModel(text: 'Delivery'),
      _TabModel(text: 'Pickup'),
    ];
    return Container(
      padding: EdgeInsets.all(3),
      decoration: BoxDecoration(
          color: kColorWhite,
          border: Border.all(color: kColorDarkGrey.withOpacity(0.3)),
          borderRadius: kBorderRadius),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: UiUtils.grigoraDynamicWidget(
            items: _items,
            builder: (position) {
              return _TabItem(
                text: _items[position].text,
                isActive: position == 0,
              );
            }),
      ),
    );
  }
}

class _TabModel {
  final String text;
  _TabModel({required this.text});
}

class _TabItem extends StatelessWidget {
  final String text;
  final bool isActive;
  const _TabItem({Key? key, this.isActive = false, required this.text})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 8, horizontal: kSpacingMedium),
      decoration: BoxDecoration(
          borderRadius: kBorderRadius,
          color: isActive ? kColorBlack : kColorWhite),
      child: GrigoraText(
        text,
        textColor: isActive ? kColorWhite : kColorBlack,
        isBoldFont: true,
      ),
    );
  }
}
