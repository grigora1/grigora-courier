import 'package:flutter/material.dart';
import 'package:courier_grigora/constants.dart';

class GrigoraText extends StatelessWidget {
  final String text;
  final TextAlign? textAlign;
  final double? textSize;
  final Color? textColor;
  final bool isCenter;
  final TextOverflow? overflow;
  final double? height;
  final int? maxLines;
  final bool? isBoldFont;
  final bool? uppercase;
  final bool isItalic;
  final double? letterSpacing;
  final TextDecoration? textDecoration;

  GrigoraText(this.text,
      {this.textAlign = TextAlign.left,
      this.textSize,
      this.textColor,
      this.maxLines,
      this.isCenter = false,
      this.overflow,
      this.letterSpacing,
      this.isItalic = false,
      this.textDecoration,
      this.uppercase = false,
      this.height,
      this.isBoldFont = false});

  @override
  Widget build(BuildContext context) {
    return Text(
      // this not clean_code but we'll name it like that
      text is String
          ? uppercase!
              ? text.toUpperCase()
              : text
          : 'no_text',
      textAlign: isCenter ? TextAlign.center : textAlign ?? TextAlign.left,
      overflow: overflow ?? TextOverflow.visible,
      maxLines: maxLines ?? 100,
      style: new TextStyle(
        fontWeight: isBoldFont! ? FontWeight.bold : FontWeight.normal,
        letterSpacing: letterSpacing ?? 0,
        color: textColor ?? kColorPrimaryText,
        fontStyle: isItalic ? FontStyle.italic : FontStyle.normal,
        decoration: textDecoration ?? TextDecoration.none,
        fontFamily: kFontFamilyRegular,
        // fontFamily: isBoldFont! ? kFontFamilyRegular : kFontFamilyLight,
        fontSize: textSize ?? kFontSizeNormal,
        height: height ?? 1.2,
      ),
    );
  }
}

class GrigoraTextTitle extends StatelessWidget {
  final String text;
  final Color? textColor;
  final bool isCenter;
  final int? maxLines;
  final TextOverflow? overflow;
  final bool isLightFont, isUpperCase;
  final double? textSize, letterSpacing;
  GrigoraTextTitle(this.text,
      {this.textColor,
      this.textSize,
      this.maxLines,
      this.overflow,
      this.isLightFont = false,
      this.isUpperCase = false,
      this.letterSpacing,
      this.isCenter = false});
  @override
  Widget build(BuildContext context) {
    return GrigoraText(
      text,
      uppercase: isUpperCase,
      maxLines: maxLines,
      overflow: overflow,
      isBoldFont: isLightFont ? false : true,
      textSize: textSize ?? kFontSizeLarge,
      letterSpacing: letterSpacing,
      textColor: textColor ?? kColorBlack,
      isCenter: isCenter,
    );
  }
}
