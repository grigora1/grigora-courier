import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:nb_utils/nb_utils.dart';

class RatingWidget extends StatelessWidget {
  const RatingWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(
          EvaIcons.star,
          color: kColorGold,
        ),
        GrigoraText(
          '4.8',
          textSize: kFontSizeLarge,
        ).paddingRight(kSpacingSmall),
        GrigoraText(
          '140+ Ratings',
        )
      ],
    );
  }
}
