import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:courier_grigora/utils/base_model.dart';
import 'package:courier_grigora/utils/base_widget.dart';
import 'package:nb_utils/nb_utils.dart';

class _VM extends BaseModel {
  int _quantity = 1;
  int get quantity => _quantity;
  set quantity(int value) {
    _quantity = value;
    notifyListeners();
  }

  increment() {
    quantity++;
    notifyListeners();
  }

  decrement() {
    if (quantity < 2) return;
    quantity--;
    notifyListeners();
  }
}

class QuantitySelectorWidget extends StatelessWidget {
  final void Function(int) callback;
  final Color? subtractBgColor;
  final bool isSmallSize;
  const QuantitySelectorWidget(
      {Key? key,
      required this.callback,
      this.subtractBgColor,
      this.isSmallSize = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseWidget<_VM>(
        model: _VM(),
        builder: (context, model, child) {
          return Row(
            children: [
              _QuantityWidgetButton(
                icon: EvaIcons.minus,
                isSmallSize: isSmallSize,
                color: subtractBgColor ?? kColorWhite,
              ).paddingRight(kSpacingSmall).onTap(() {
                model.decrement();
                callback.call(model.quantity);
              }),
              GrigoraTextTitle(model.quantity.toString())
                  .paddingRight(kSpacingSmall),
              _QuantityWidgetButton(
                icon: EvaIcons.plus,
                color: kColorBlack,
                isSmallSize: isSmallSize,
                textColor: kColorWhite,
              ).onTap(() {
                model.increment();
                callback.call(model.quantity);
              }),
            ],
          );
        });
  }
}

class _QuantityWidgetButton extends StatelessWidget {
  final Color? color, textColor;
  final bool isSmallSize;
  final IconData icon;
  const _QuantityWidgetButton(
      {Key? key,
      this.color,
      this.textColor,
      this.isSmallSize = false,
      required this.icon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: isSmallSize ? EdgeInsets.all(8) : kPaddingAllSmall,
      decoration: BoxDecoration(
          color: color ?? kColorDarkGrey.withOpacity(0.1),
          borderRadius: kBorderRadius),
      child: Icon(
        icon,
        size: isSmallSize ? 10 : kFontSizeNormal,
        color: textColor ?? kColorBlack,
      ),
    );
  }
}
