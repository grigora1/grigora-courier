import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:courier_grigora/utils/form_item.dart';
import 'package:nb_utils/nb_utils.dart';

class AdditionalMessage extends StatelessWidget {
  final String? title;
  const AdditionalMessage({Key? key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Icon(EvaIcons.messageSquareOutline).paddingRight(kSpacingSmall),
            GrigoraTextTitle(
              title ?? 'Additional Message',
              textSize: kFontSizeMedium,
            )
          ],
        ),
        FormItem(
          isMultiline: true,
          placeholder: 'Anything else we need to know?',
        ).paddingTop(kSpacingSmall),
      ],
    );
  }
}
