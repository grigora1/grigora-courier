import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';

class HorizontalDivider extends StatelessWidget {
  const HorizontalDivider({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: kWidthFull(context),
      height: 1,
      color: kColorBlack.withOpacity(0.2),
    );
  }
}

class DashboardCustomTitle extends StatelessWidget {
  final String title, subtitle;
  const DashboardCustomTitle(
      {Key? key, required this.title, required this.subtitle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GrigoraTextTitle(
          subtitle,
          isCenter: true,
          textColor: kColorPrimary,
        ),
        GrigoraTextTitle(
          title,
          textColor: kColorAccent,
          textSize: 45,
        ).paddingTop(kSpacingSmall),
      ],
    );
  }
}

class DashboardColumnItem extends StatelessWidget {
  final String title, value;
  final Color? color;
  const DashboardColumnItem(
      {Key? key, required this.title, this.color, required this.value})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GrigoraText(
          title,
          isBoldFont: true,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          isCenter: true,
        ),
        GrigoraTextTitle(
          value,
          textSize: kFontSizeBig,
        ).paddingTop(kSpacingSmall),
      ],
    ).paddingAll(kSpacingMedium);
  }
}

class DashboardTextArrow extends StatelessWidget {
  final String text;
  const DashboardTextArrow({Key? key, required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        GrigoraText(text).expand(),
        Icon(
          EvaIcons.chevronRight,
          size: 32,
        )
      ],
    );
  }
}
