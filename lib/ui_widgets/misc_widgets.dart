import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/buttons.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:courier_grigora/ui_widgets/ui_utils.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';

List<Widget> miscWidgetAppbarActions = [
  Icon(
    EvaIcons.list,
    size: 24,
  )
      .onTap(() => UiUtils.goto(kRouteCurrentSchedule))
      .paddingRight(kSpacingSmall),
  Icon(
    EvaIcons.questionMarkCircleOutline,
    color: kColorPrimary,
    size: 24,
  ).onTap(() => UiUtils.goto(kRoutePostmanHelp)).paddingRight(kSpacingMedium)
];

class MiscWidgetCheckboxItem extends StatelessWidget {
  final String text;
  const MiscWidgetCheckboxItem({Key? key, required this.text})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Checkbox(value: false, onChanged: (value) => print(value)),
        GrigoraText(text),
      ],
    );
  }
}

class MiscWidgetDeliveryCompleteItems extends StatelessWidget {
  const MiscWidgetDeliveryCompleteItems({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          GrigoraTextTitle(
            'Delivery Complete',
            textSize: kFontSizeBig,
            isCenter: true,
          ),
          _DeliveryCompleteItems().paddingTop(kSpacingMedium),
          _DeliveryCompleteInfoWidget().paddingTop(kSpacingMedium),
          GrigoraButton(
            onTap: () => UiUtils.goto(kRouteDashboard),
            text: 'Okay',
            withBg: true,
          ).paddingTop(kSpacingMedium)
        ],
      ),
    );
  }
}

class _DeliveryCompleteInfoWidget extends StatelessWidget {
  const _DeliveryCompleteInfoWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(Icons.payment).paddingRight(kSpacingMedium),
        GrigoraText('Total is higher than the N200 shown on acceptance')
            .expand()
      ],
    );
  }
}

class _DeliveryCompleteItems extends StatelessWidget {
  const _DeliveryCompleteItems({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _items = [
      {'title': 'Dzooco Pay', 'subtitle': 'Base pay', 'price': 'N2000'},
      {'title': 'Tip', 'subtitle': 'Customer tip', 'price': 'N2000'},
    ];
    return Column(
      children: UiUtils.grigoraDynamicWidget(
          items: _items,
          builder: (i) => Row(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      GrigoraTextTitle(_items[i]['title'] ?? 'no_title'),
                      GrigoraText(
                        _items[i]['subtitle'] ?? 'no_subtitle',
                        textColor: kColorPrimary,
                        isBoldFont: true,
                      )
                    ],
                  ).expand(),
                  GrigoraText(_items[i]['price'] ?? 'no_price')
                ],
              ).paddingTop(kSpacingMedium)),
    );
  }
}
