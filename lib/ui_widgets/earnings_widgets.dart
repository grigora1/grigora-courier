import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/dashboard_widgets.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';

class EarningItem extends StatelessWidget {
  final String title, subtitle, value;
  final bool isLastItem;
  const EarningItem(
      {Key? key,
      required this.title,
      this.isLastItem = false,
      required this.subtitle,
      required this.value})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GrigoraText(
                title,
                isBoldFont: true,
              ),
              GrigoraText(
                subtitle,
                textSize: kFontSizeSmall,
              ).paddingTop(kSpacingSmall),
            ],
          ).expand(),
          GrigoraTextTitle(
            value,
          ),
          Icon(
            EvaIcons.chevronRightOutline,
            size: 32,
          )
        ]).paddingSymmetric(vertical: kSpacingSmall),
        HorizontalDivider().visible(!isLastItem)
      ],
    );
  }
}

class EarningsCol2Table extends StatelessWidget {
  final String leftTitle, leftValue, rightTitle, rightValue;
  const EarningsCol2Table(
      {Key? key,
      required this.leftTitle,
      required this.leftValue,
      required this.rightTitle,
      required this.rightValue})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        DashboardColumnItem(title: leftTitle, value: leftValue).expand(),
        Container(
          height: 70,
          width: 1,
          color: kColorBlack.withOpacity(0.2),
        ),
        DashboardColumnItem(title: rightTitle, value: rightValue).expand()
      ],
    );
  }
}
