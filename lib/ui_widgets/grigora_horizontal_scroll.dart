import 'package:flutter/material.dart';
import 'package:courier_grigora/constants.dart';

class HorizontalScrollModel {}

class GrigoraHorizontalScroll<T extends HorizontalScrollModel>
    extends StatelessWidget {
  final List<T> items;
  final Widget Function(int position, T model) builder;
  const GrigoraHorizontalScroll(
      {Key? key, required this.builder, required this.items})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          physics: BouncingScrollPhysics(),
          child: Padding(
            padding: const EdgeInsets.only(left: kSpacingMedium),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: items
                  .asMap()
                  .map((int position, T model) =>
                      MapEntry(position, builder(position, model)))
                  .values
                  .toList(),
            ),
          ),
        ),
      ],
    );
  }
}
