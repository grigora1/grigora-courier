import 'package:courier_grigora/utils/base_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:provider/provider.dart';

class UiUtils {
  static showSnackbar(String message) => GetBar(
        message: message,
        margin: const EdgeInsets.all(8),
        dismissDirection: SnackDismissDirection.HORIZONTAL,
        borderRadius: 8,
        isDismissible: true,
        animationDuration: Duration(milliseconds: 500),
        duration: Duration(seconds: 2),
      )..show();

  static showNotification(
          {String title = 'no_title', String message = 'no_message'}) =>
      Get.snackbar(title, message);

  static showDialog(Widget widget) => Get.dialog(AlertDialog(
        content: widget,
      ));

  static showSimpleDialog(
          {required String title, message, required Function()? onOkPressed}) =>
      Get.dialog(AlertDialog(
        titlePadding:
            EdgeInsets.only(top: kSpacingMedium, left: kSpacingMedium),
        contentPadding: kPaddingAllMedium,
        backgroundColor: kColorWhite,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10))),
        title: GrigoraTextTitle(title),
        content: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            GrigoraText(message),
            SizedBox(height: 16),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                InkWell(
                  onTap: onOkPressed,
                  child: GrigoraText(
                    "Ok",
                    textColor: kColorPrimary,
                  ),
                )
              ],
            )
          ],
        ),
        buttonPadding: EdgeInsets.zero,
      ));

  static showBottomSheetModal(
          {required BuildContext context, required Widget widget}) =>
      showModalBottomSheet(
          context: context,
          backgroundColor: kColorWhite,
          isScrollControlled: true,
          shape: RoundedRectangleBorder(
              borderRadius:
                  BorderRadius.vertical(top: Radius.circular(kSpacingSmall))),
          builder: (BuildContext context) {
            return Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  widget,
                ],
              ),
            );
          });

  static showBottomSheet<T extends BaseModel>(
          {required Widget body, T? model}) =>
      Get.bottomSheet(body,
          backgroundColor: kColorWhite,
          shape: RoundedRectangleBorder(
              borderRadius:
                  BorderRadius.vertical(top: Radius.circular(kSpacingSmall))));

  static goto(String pageRoute, {dynamic arguments}) =>
      Get.toNamed(pageRoute, arguments: arguments);

  static goToWidget(Widget widget) => Get.to(() => widget);

  static goBack() => Get.back();

  static navigatorKey() => Get.key;

  static DecorationImage grigoraNetworkImageProvider(String? image) =>
      DecorationImage(
          fit: BoxFit.cover,
          image: CachedNetworkImageProvider(image ?? kImagesRemotePlaceholder));

  static Widget grigoraNetworkImageWithContainer(String? networkImage,
          {double? width, double? height}) =>
      CachedNetworkImage(
        imageUrl: networkImage ?? kImagesRemotePlaceholder,
        imageBuilder: (context, imageProvider) => Container(
          width: width ?? MediaQuery.of(context).size.width,
          height: height ?? MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: imageProvider,
              fit: BoxFit.cover,
            ),
          ),
        ),
        placeholder: (context, url) => CircularProgressIndicator(),
        errorWidget: (context, url, error) => Icon(Icons.error),
      );

  static List<Widget> grigoraDynamicWidget(
      {required List<dynamic> items,
      required Widget Function(int position) builder}) {
    return items
        .asMap()
        .map((int position, _) => MapEntry(position, builder(position)))
        .values
        .toList();
  }
}
