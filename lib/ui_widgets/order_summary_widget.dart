import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:nb_utils/nb_utils.dart';

class OrderSummaryWidget extends StatelessWidget {
  const OrderSummaryWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GrigoraTextTitle(
          'Order Summary',
          textSize: kFontSizeMedium,
        ).paddingBottom(kSpacingSmall),
        Table(
          border: TableBorder(
              horizontalInside:
                  BorderSide(color: kColorDarkGrey.withOpacity(0.1))),
          columnWidths: const <int, TableColumnWidth>{
            0: FlexColumnWidth(),
            1: FlexColumnWidth(),
          },
          defaultVerticalAlignment: TableCellVerticalAlignment.middle,
          children: <TableRow>[
            TableRow(
                decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(
                            color: kColorDarkGrey.withOpacity(0.1)))),
                children: [
                  GrigoraText(
                    'Subtotal',
                    textSize: kFontSizeMedium,
                  ),
                  GrigoraText(
                    'NGN 6000',
                    textSize: kFontSizeMedium,
                    textAlign: TextAlign.end,
                  ).paddingSymmetric(vertical: kSpacingSmall),
                ]),
            TableRow(children: [
              GrigoraText(
                'Discount',
                textSize: kFontSizeMedium,
              ),
              GrigoraText(
                'NGN 600',
                textSize: kFontSizeMedium,
                textAlign: TextAlign.end,
              ).paddingSymmetric(vertical: kSpacingSmall),
            ]),
            TableRow(children: [
              Row(
                children: [
                  GrigoraText(
                    'Service Charge',
                    textSize: kFontSizeMedium,
                  ),
                  Icon(
                    EvaIcons.info,
                    color: kColorDarkGrey.withOpacity(0.2),
                  ).paddingRight(kSpacingSmall)
                ],
              ),
              GrigoraText(
                'NGN 600',
                textSize: kFontSizeMedium,
                textAlign: TextAlign.end,
              ).paddingSymmetric(vertical: kSpacingSmall),
            ]),
            TableRow(children: [
              GrigoraText(
                'Total',
                textSize: kFontSizeMedium,
                isBoldFont: true,
              ),
              GrigoraText(
                'NGN 5400',
                textSize: kFontSizeMedium,
                textAlign: TextAlign.end,
                isBoldFont: true,
              ).paddingSymmetric(vertical: kSpacingSmall),
            ]),
          ],
        )
      ],
    );
  }
}
