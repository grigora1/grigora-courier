import 'package:flutter/material.dart';
import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/text.dart';

class LayoutWithoutBack extends StatelessWidget {
  final Widget body;
  final String title;
  final bool withStack;
  final Widget? actionWidget;
  final Color? bgColor;
  const LayoutWithoutBack(
      {Key? key,
      this.withStack = false,
      this.actionWidget,
      this.bgColor,
      required this.body,
      required this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        iconTheme: IconThemeData(color: kColorBlack),
        title: GrigoraTextTitle(
          title,
          textSize: kFontSizeBig,
        ),
        actions: [actionWidget ?? Container()],
        elevation: 0,
        backgroundColor: bgColor ?? kColorBg,
      ),
      body: withStack
          ? body
          : Container(
              color: bgColor ?? kColorBg,
              width: kWidthFull(context),
              height: kHeightFull(context),
              child: SingleChildScrollView(
                child: body,
              ),
            ),
    );
  }
}
