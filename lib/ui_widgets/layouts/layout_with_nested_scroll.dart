import 'package:flutter/material.dart';
import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/text.dart';

class LayoutWithNestedScroll extends StatelessWidget {
  final Widget body, backgroundWidget;
  final double? expandedHeight;
  final String? title;
  final Widget? buttonWidget;
  final Color? bgColor;
  final List<Widget>? actions;
  const LayoutWithNestedScroll(
      {Key? key,
      required this.body,
      this.expandedHeight,
      this.title,
      this.buttonWidget,
      this.bgColor,
      this.actions,
      required this.backgroundWidget})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
          backgroundColor: bgColor,
          body: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  expandedHeight: expandedHeight ?? 150,
                  floating: false,
                  title: GrigoraTextTitle(title ?? ''),
                  pinned: true,
                  actions: actions,
                  iconTheme: IconThemeData(color: kColorBlack),
                  // elevation: 0,
                  backgroundColor: kColorWhite,
                  flexibleSpace: FlexibleSpaceBar(
                    centerTitle: true,
                    title: GrigoraTextTitle(''),
                    background: backgroundWidget,
                  ),
                )
              ];
            },
            body: Stack(
              children: [
                SingleChildScrollView(
                  child: body,
                ),
                Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    child: buttonWidget is Widget
                        ? Padding(
                            padding: EdgeInsets.all(kSpacingMedium),
                            child: buttonWidget,
                          )
                        : Container())
              ],
            ),
          )),
    );
  }
}
