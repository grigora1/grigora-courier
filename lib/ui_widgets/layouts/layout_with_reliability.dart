import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/buttons.dart';
import 'package:courier_grigora/ui_widgets/layouts/layout_with_back.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:courier_grigora/ui_widgets/ui_utils.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';

class LayoutWithReliability extends StatelessWidget {
  final String title, description, reliabilityScore, buttonText;
  final bool isReliability;
  const LayoutWithReliability(
      {Key? key,
      required this.title,
      required this.description,
      required this.buttonText,
      this.isReliability = false,
      required this.reliabilityScore})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutWithBack(
      title: '',
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GrigoraTextTitle(title),
          GrigoraText(description).paddingTop(kSpacingSmall),
          _SpecialText().paddingTop(kSpacingMedium),
          _ReliabilityScore(score: reliabilityScore)
              .center()
              .paddingTop(kSpacingMedium),
          GrigoraButton(
            onTap: () => UiUtils.goto(kRouteDashboard),
            text: buttonText,
            withBg: true,
          ).paddingTop(kSpacingExtraLarge),
        ],
      ).paddingAll(kSpacingMedium),
    );
  }
}

class _ReliabilityScore extends StatelessWidget {
  final String score;
  final bool isReliability;
  const _ReliabilityScore(
      {Key? key, required this.score, this.isReliability = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: kColorWhite,
          borderRadius: kBorderRadius,
          boxShadow: [kBoxShadow(kColorBlack.withOpacity(0.1))]),
      child: Column(
        children: [
          GrigoraTextTitle(
            score,
            textSize: 40,
            textColor: kColorSecondary,
          ),
          GrigoraText(
            isReliability ? 'Reliability' : 'Acceptance',
            isBoldFont: true,
          ).paddingTop(kSpacingSmall)
        ],
      ).paddingAll(kSpacingBigMedium),
    );
  }
}

class _SpecialText extends StatelessWidget {
  final bool isReliability;
  const _SpecialText({Key? key, this.isReliability = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: new TextSpan(
        style: kBodyTextBold,
        children: <TextSpan>[
          TextSpan(
            text: 'Your ',
          ),
          TextSpan(
              text: '${isReliability ? 'reliability' : 'acceptance'} score ',
              style: new TextStyle(
                color: kColorPrimary,
              )),
          TextSpan(
            text: 'will drop to:',
          ),
        ],
      ),
    );
  }
}
