import 'package:flutter/material.dart';
import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/text.dart';

class LayoutWithBack extends StatelessWidget {
  final Widget body;
  final String title;
  final bool withStack;
  final Color? bgColor;
  final double? elevation;
  final List<Widget>? actions;
  const LayoutWithBack(
      {Key? key,
      this.withStack = false,
      this.bgColor,
      this.actions,
      this.elevation,
      required this.body,
      required this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor ?? kColorWhite,
      appBar: new AppBar(
        iconTheme: IconThemeData(color: kColorBlack),
        title: GrigoraTextTitle(title),
        actions: actions,
        // centerTitle: true,
        elevation: elevation ?? 0,
        backgroundColor: bgColor ?? kColorWhite,
      ),
      body: withStack
          ? body
          : SingleChildScrollView(
              child: body,
            ),
    );
  }
}
