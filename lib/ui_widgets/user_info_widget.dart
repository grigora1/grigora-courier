import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:courier_grigora/ui_widgets/ui_utils.dart';
import 'package:nb_utils/nb_utils.dart';

class UserInfoWidget extends StatelessWidget {
  const UserInfoWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        UiUtils.grigoraNetworkImageWithContainer(
                'https://res.cloudinary.com/dbdpkkfaf/image/upload/v1627470541/grigora/Rectangle_32_ukalim.png',
                width: 40,
                height: 40)
            .paddingRight(kSpacingSmall),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GrigoraText(
              'John Cather',
              textSize: kFontSizeMedium,
            ),
            GrigoraText(
              'Driver ID: 8632697',
            ),
          ],
        ).expand(),
        Row(
          children: [
            _UserIcon(icon: EvaIcons.phoneOutline),
            _UserIcon(icon: EvaIcons.messageSquareOutline)
                .paddingLeft(kSpacingSmall),
          ],
        )
      ],
    );
  }
}

class _UserIcon extends StatelessWidget {
  final IconData icon;
  const _UserIcon({Key? key, required this.icon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: kColorGrey.withOpacity(0.3), borderRadius: kBorderRadius),
      child: Icon(
        icon,
        color: kColorPrimary,
      ).paddingAll(kSpacingSmall),
    );
  }
}
