import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';

class EmptyPlaceholder extends StatelessWidget {
  final String text;
  const EmptyPlaceholder({Key? key, required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: kColorGrey,
      width: kWidthFull(context),
      height: kHeightFull(context),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(kImagesEmpty),
          GrigoraTextTitle(
            text,
            isCenter: true,
            textColor: kColorBlack.withOpacity(0.5),
          ).paddingTop(kSpacingMedium),
          Container(
            height: 100,
          )
        ],
      ),
    );
  }
}
