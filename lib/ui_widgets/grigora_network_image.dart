import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:courier_grigora/constants.dart';

class GrigoraNetworkImage extends StatelessWidget {
  final String? networkImage;
  const GrigoraNetworkImage(this.networkImage, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: networkImage ?? kImagesRemotePlaceholder,
      // width: width ?? kWidthFull(context),
      // height: 40,
      progressIndicatorBuilder: (context, url, downloadProgress) =>
          CircularProgressIndicator(value: downloadProgress.progress),
      errorWidget: (context, url, error) => Icon(Icons.error),
    );
  }
}
