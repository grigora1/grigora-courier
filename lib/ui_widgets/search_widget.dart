import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/utils/form_item.dart';
import 'package:nb_utils/nb_utils.dart';

class SearchWidget extends StatelessWidget {
  const SearchWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: 60,
      // padding: kPaddingAllSmall,
      decoration: BoxDecoration(
          color: kColorWhite,
          border: Border.all(color: kColorBlack.withOpacity(0.1)),
          borderRadius: kBorderRadius),
      child: Row(
        children: [
          Icon(
            EvaIcons.searchOutline,
            color: kColorDarkGrey.withOpacity(0.5),
          ).paddingRight(kSpacingSmall),
          FormItem(
            placeholder: 'Search for an item',
            withNoBorder: true,
          ).expand(),
          Icon(
            EvaIcons.micOutline,
            color: kColorPrimary,
          )
        ],
      ).paddingSymmetric(horizontal: kSpacingSmall, vertical: 5),
    );
  }
}
