import 'package:flutter/material.dart';
import 'package:courier_grigora/ui_widgets/text.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:courier_grigora/constants.dart';

class GrigoraSectionWidget extends StatelessWidget {
  final Widget child;
  final String? title;
  final bool hideTitle;
  final Function? showAllAction;
  const GrigoraSectionWidget({
    Key? key,
    required this.child,
    this.title,
    this.hideTitle = false,
    this.showAllAction,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Visibility(
          visible: !hideTitle,
          child: Row(
            children: [
              Expanded(child: GrigoraTextTitle(title ?? 'no_title')),
              GrigoraText(
                'Show All',
                textColor: kColorPrimary,
                isBoldFont: true,
              ).visible(showAllAction != null).onTap(showAllAction)
            ],
          )
              .paddingSymmetric(horizontal: kSpacingMedium)
              .paddingBottom(kSpacingMedium),
        ),
        child
      ],
    );
  }
}
