import 'package:flutter/material.dart';
import 'package:courier_grigora/constants.dart';
import 'package:courier_grigora/utils/base_model.dart';
import 'package:provider/provider.dart';

class FunctionHelpers {
  // @todo: delete
  static void openBottomSheet<T extends BaseModel>(
      {required BuildContext context,
      T? model,
      required Widget child,
      Color? bgColor}) {
    showModalBottomSheet(
        context: context,
        backgroundColor: bgColor ?? kColorDarkGrey,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(16))),
        builder: (BuildContext context) {
          return ChangeNotifierProvider.value(
              value: model,
              child: Container(
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).viewInsets.bottom),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Container(
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            InkWell(
                              // onTap: () => Helpers.goback(),
                              child: Container(
                                color: Colors.black12,
                                padding: EdgeInsets.all(1),
                                width: MediaQuery.of(context).size.width,
                                child: Icon(
                                  Icons.arrow_drop_down,
                                  size: 30,
                                  color: Colors.black26,
                                ),
                              ),
                            ),
                            child
                          ],
                        )),
                  ],
                ),
              ));
        });
  }
}
