import 'package:courier_grigora/utils/app_enums.dart';
import 'package:flutter/cupertino.dart';

class BaseModel extends ChangeNotifier {
  EnumHomeState _state = EnumHomeState.postman;
  EnumHomeState get state => _state;
  set state(EnumHomeState value) {
    _state = value;
    notifyListeners();
  }

  bool showDefaultHeaderWidget() {
    switch (state) {
      case EnumHomeState.navigate:
        return false;
      case EnumHomeState.orderSearch:
        return false;
      default:
        return true;
    }
  }
}
