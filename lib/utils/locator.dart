import 'package:get_it/get_it.dart';
import 'package:courier_grigora/services/auth_services.dart';
import 'package:courier_grigora/services/hardware_info_service.dart';
import 'package:courier_grigora/services/http_services.dart';
import 'package:courier_grigora/services/navigation_service.dart';

GetIt locator = GetIt.instance;

Future<void> setupLocator() async {
  locator.registerLazySingleton<AuthServices>(() => AuthServicesImpl());
  locator.registerFactory<HttpServices>(() => HttpServicesImpl());
  locator.registerFactory<NavigationService>(() => NavigationServiceImpl());
  locator.registerLazySingleton<HardwareInfoService>(
    () => HardwareInfoServiceImpl(),
  );
  // @todo: implement log_service
}
